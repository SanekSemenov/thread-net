﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Thread_.NET.Common.DTO.Comment
{
    public sealed class CommentUpdateDTO 
    {
        public int Id { get; set; }
        public int PostId { get; set; }
        public string Body { get; set; }
    }
}
