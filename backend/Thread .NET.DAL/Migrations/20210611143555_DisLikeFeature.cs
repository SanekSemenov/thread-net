﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Thread_.NET.DAL.Migrations
{
    public partial class DisLikeFeature : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 20);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 20);

            migrationBuilder.AddColumn<bool>(
                name: "IsDisLike",
                table: "PostReactions",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsDisLike",
                table: "CommentReactions",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.InsertData(
                table: "CommentReactions",
                columns: new[] { "Id", "CommentId", "CreatedAt", "IsDisLike", "IsLike", "UpdatedAt", "UserId" },
                values: new object[,]
                {
                    { 1, 2, new DateTime(2021, 6, 11, 17, 35, 55, 7, DateTimeKind.Local).AddTicks(9492), false, true, new DateTime(2021, 6, 11, 17, 35, 55, 8, DateTimeKind.Local).AddTicks(703), 14 },
                    { 18, 18, new DateTime(2021, 6, 11, 17, 35, 55, 8, DateTimeKind.Local).AddTicks(2849), false, true, new DateTime(2021, 6, 11, 17, 35, 55, 8, DateTimeKind.Local).AddTicks(2858), 19 },
                    { 17, 14, new DateTime(2021, 6, 11, 17, 35, 55, 8, DateTimeKind.Local).AddTicks(2786), false, true, new DateTime(2021, 6, 11, 17, 35, 55, 8, DateTimeKind.Local).AddTicks(2797), 21 },
                    { 15, 14, new DateTime(2021, 6, 11, 17, 35, 55, 8, DateTimeKind.Local).AddTicks(2659), true, true, new DateTime(2021, 6, 11, 17, 35, 55, 8, DateTimeKind.Local).AddTicks(2673), 4 },
                    { 14, 12, new DateTime(2021, 6, 11, 17, 35, 55, 8, DateTimeKind.Local).AddTicks(2590), false, false, new DateTime(2021, 6, 11, 17, 35, 55, 8, DateTimeKind.Local).AddTicks(2603), 13 },
                    { 13, 11, new DateTime(2021, 6, 11, 17, 35, 55, 8, DateTimeKind.Local).AddTicks(2254), true, false, new DateTime(2021, 6, 11, 17, 35, 55, 8, DateTimeKind.Local).AddTicks(2263), 17 },
                    { 12, 2, new DateTime(2021, 6, 11, 17, 35, 55, 8, DateTimeKind.Local).AddTicks(2192), false, true, new DateTime(2021, 6, 11, 17, 35, 55, 8, DateTimeKind.Local).AddTicks(2201), 20 },
                    { 11, 7, new DateTime(2021, 6, 11, 17, 35, 55, 8, DateTimeKind.Local).AddTicks(2121), false, false, new DateTime(2021, 6, 11, 17, 35, 55, 8, DateTimeKind.Local).AddTicks(2133), 1 },
                    { 19, 2, new DateTime(2021, 6, 11, 17, 35, 55, 8, DateTimeKind.Local).AddTicks(2911), true, false, new DateTime(2021, 6, 11, 17, 35, 55, 8, DateTimeKind.Local).AddTicks(2921), 15 },
                    { 10, 17, new DateTime(2021, 6, 11, 17, 35, 55, 8, DateTimeKind.Local).AddTicks(2049), false, false, new DateTime(2021, 6, 11, 17, 35, 55, 8, DateTimeKind.Local).AddTicks(2061), 10 },
                    { 8, 13, new DateTime(2021, 6, 11, 17, 35, 55, 8, DateTimeKind.Local).AddTicks(1924), true, false, new DateTime(2021, 6, 11, 17, 35, 55, 8, DateTimeKind.Local).AddTicks(1931), 3 },
                    { 7, 16, new DateTime(2021, 6, 11, 17, 35, 55, 8, DateTimeKind.Local).AddTicks(1850), true, false, new DateTime(2021, 6, 11, 17, 35, 55, 8, DateTimeKind.Local).AddTicks(1863), 15 },
                    { 6, 4, new DateTime(2021, 6, 11, 17, 35, 55, 8, DateTimeKind.Local).AddTicks(1789), false, true, new DateTime(2021, 6, 11, 17, 35, 55, 8, DateTimeKind.Local).AddTicks(1798), 21 },
                    { 5, 11, new DateTime(2021, 6, 11, 17, 35, 55, 8, DateTimeKind.Local).AddTicks(1721), false, false, new DateTime(2021, 6, 11, 17, 35, 55, 8, DateTimeKind.Local).AddTicks(1727), 5 },
                    { 4, 16, new DateTime(2021, 6, 11, 17, 35, 55, 8, DateTimeKind.Local).AddTicks(1651), false, false, new DateTime(2021, 6, 11, 17, 35, 55, 8, DateTimeKind.Local).AddTicks(1662), 13 },
                    { 3, 3, new DateTime(2021, 6, 11, 17, 35, 55, 8, DateTimeKind.Local).AddTicks(1586), true, true, new DateTime(2021, 6, 11, 17, 35, 55, 8, DateTimeKind.Local).AddTicks(1598), 4 },
                    { 2, 7, new DateTime(2021, 6, 11, 17, 35, 55, 8, DateTimeKind.Local).AddTicks(1508), true, false, new DateTime(2021, 6, 11, 17, 35, 55, 8, DateTimeKind.Local).AddTicks(1523), 15 },
                    { 9, 17, new DateTime(2021, 6, 11, 17, 35, 55, 8, DateTimeKind.Local).AddTicks(1989), true, true, new DateTime(2021, 6, 11, 17, 35, 55, 8, DateTimeKind.Local).AddTicks(1997), 8 },
                    { 20, 2, new DateTime(2021, 6, 11, 17, 35, 55, 8, DateTimeKind.Local).AddTicks(2972), false, true, new DateTime(2021, 6, 11, 17, 35, 55, 8, DateTimeKind.Local).AddTicks(2986), 21 },
                    { 16, 19, new DateTime(2021, 6, 11, 17, 35, 55, 8, DateTimeKind.Local).AddTicks(2725), false, true, new DateTime(2021, 6, 11, 17, 35, 55, 8, DateTimeKind.Local).AddTicks(2732), 15 }
                });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 11, "Porro vitae eius ea error voluptates reprehenderit ipsum.", new DateTime(2021, 6, 11, 17, 35, 54, 987, DateTimeKind.Local).AddTicks(7024), 20, new DateTime(2021, 6, 11, 17, 35, 54, 987, DateTimeKind.Local).AddTicks(8142) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 12, "Deleniti eaque ducimus.", new DateTime(2021, 6, 11, 17, 35, 54, 987, DateTimeKind.Local).AddTicks(9412), 6, new DateTime(2021, 6, 11, 17, 35, 54, 987, DateTimeKind.Local).AddTicks(9432) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 6, "Dignissimos vel est veniam consectetur est blanditiis quam aperiam aspernatur.", new DateTime(2021, 6, 11, 17, 35, 54, 987, DateTimeKind.Local).AddTicks(9634), 10, new DateTime(2021, 6, 11, 17, 35, 54, 987, DateTimeKind.Local).AddTicks(9646) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 10, "Blanditiis nisi reiciendis.", new DateTime(2021, 6, 11, 17, 35, 54, 987, DateTimeKind.Local).AddTicks(9749), 2, new DateTime(2021, 6, 11, 17, 35, 54, 987, DateTimeKind.Local).AddTicks(9764) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 9, "Iste omnis minus modi sunt quia blanditiis autem praesentium perspiciatis.", new DateTime(2021, 6, 11, 17, 35, 54, 988, DateTimeKind.Local).AddTicks(512), 12, new DateTime(2021, 6, 11, 17, 35, 54, 988, DateTimeKind.Local).AddTicks(543) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 8, "Odio provident voluptas cupiditate harum iure non.", new DateTime(2021, 6, 11, 17, 35, 54, 988, DateTimeKind.Local).AddTicks(723), 16, new DateTime(2021, 6, 11, 17, 35, 54, 988, DateTimeKind.Local).AddTicks(736) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 15, "Provident consequatur ratione optio dolores molestias qui et mollitia commodi.", new DateTime(2021, 6, 11, 17, 35, 54, 988, DateTimeKind.Local).AddTicks(899), 15, new DateTime(2021, 6, 11, 17, 35, 54, 988, DateTimeKind.Local).AddTicks(907) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 8, "Impedit nesciunt velit nesciunt minus.", new DateTime(2021, 6, 11, 17, 35, 54, 988, DateTimeKind.Local).AddTicks(1036), 20, new DateTime(2021, 6, 11, 17, 35, 54, 988, DateTimeKind.Local).AddTicks(1045) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 12, "Laborum perspiciatis quidem aperiam dolores laborum illum nisi illo iste.", new DateTime(2021, 6, 11, 17, 35, 54, 988, DateTimeKind.Local).AddTicks(1231), 16, new DateTime(2021, 6, 11, 17, 35, 54, 988, DateTimeKind.Local).AddTicks(1246) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 1, "Sed velit molestiae molestiae sapiente accusamus minus.", new DateTime(2021, 6, 11, 17, 35, 54, 988, DateTimeKind.Local).AddTicks(1531), 17, new DateTime(2021, 6, 11, 17, 35, 54, 988, DateTimeKind.Local).AddTicks(1547) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 8, "Itaque est sed dolores.", new DateTime(2021, 6, 11, 17, 35, 54, 988, DateTimeKind.Local).AddTicks(1671), 9, new DateTime(2021, 6, 11, 17, 35, 54, 988, DateTimeKind.Local).AddTicks(1687) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 20, "Autem perferendis est odit.", new DateTime(2021, 6, 11, 17, 35, 54, 988, DateTimeKind.Local).AddTicks(1798), 6, new DateTime(2021, 6, 11, 17, 35, 54, 988, DateTimeKind.Local).AddTicks(1815) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 3, "Distinctio alias saepe voluptates nesciunt perspiciatis consequatur.", new DateTime(2021, 6, 11, 17, 35, 54, 988, DateTimeKind.Local).AddTicks(1950), 19, new DateTime(2021, 6, 11, 17, 35, 54, 988, DateTimeKind.Local).AddTicks(1962) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 8, "Molestias maxime assumenda minus est voluptatum voluptatem fugiat temporibus.", new DateTime(2021, 6, 11, 17, 35, 54, 988, DateTimeKind.Local).AddTicks(2106), 15, new DateTime(2021, 6, 11, 17, 35, 54, 988, DateTimeKind.Local).AddTicks(2115) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 16, "Nostrum deleniti qui voluptates eum.", new DateTime(2021, 6, 11, 17, 35, 54, 988, DateTimeKind.Local).AddTicks(2230), 11, new DateTime(2021, 6, 11, 17, 35, 54, 988, DateTimeKind.Local).AddTicks(2239) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 20, "Laudantium veniam ratione.", new DateTime(2021, 6, 11, 17, 35, 54, 988, DateTimeKind.Local).AddTicks(2342), 16, new DateTime(2021, 6, 11, 17, 35, 54, 988, DateTimeKind.Local).AddTicks(2355) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 14, "Aut qui rerum minima eum quibusdam et ducimus et dolorum.", new DateTime(2021, 6, 11, 17, 35, 54, 988, DateTimeKind.Local).AddTicks(2736), 10, new DateTime(2021, 6, 11, 17, 35, 54, 988, DateTimeKind.Local).AddTicks(2749) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 3, "Sed excepturi enim.", new DateTime(2021, 6, 11, 17, 35, 54, 988, DateTimeKind.Local).AddTicks(2858), 19, new DateTime(2021, 6, 11, 17, 35, 54, 988, DateTimeKind.Local).AddTicks(2870) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 7, "Aliquid impedit esse blanditiis dolore porro quae et et.", new DateTime(2021, 6, 11, 17, 35, 54, 988, DateTimeKind.Local).AddTicks(3021), 5, new DateTime(2021, 6, 11, 17, 35, 54, 988, DateTimeKind.Local).AddTicks(3032) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 21, "Pariatur delectus vel et odio rerum molestias.", new DateTime(2021, 6, 11, 17, 35, 54, 988, DateTimeKind.Local).AddTicks(3162), 19, new DateTime(2021, 6, 11, 17, 35, 54, 988, DateTimeKind.Local).AddTicks(3175) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 11, 17, 35, 54, 681, DateTimeKind.Local).AddTicks(5873), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/902.jpg", new DateTime(2021, 6, 11, 17, 35, 54, 682, DateTimeKind.Local).AddTicks(366) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 11, 17, 35, 54, 682, DateTimeKind.Local).AddTicks(1140), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/1180.jpg", new DateTime(2021, 6, 11, 17, 35, 54, 682, DateTimeKind.Local).AddTicks(1146) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 11, 17, 35, 54, 682, DateTimeKind.Local).AddTicks(1169), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/425.jpg", new DateTime(2021, 6, 11, 17, 35, 54, 682, DateTimeKind.Local).AddTicks(1172) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 11, 17, 35, 54, 682, DateTimeKind.Local).AddTicks(1186), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/818.jpg", new DateTime(2021, 6, 11, 17, 35, 54, 682, DateTimeKind.Local).AddTicks(1190) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 11, 17, 35, 54, 682, DateTimeKind.Local).AddTicks(1204), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/754.jpg", new DateTime(2021, 6, 11, 17, 35, 54, 682, DateTimeKind.Local).AddTicks(1207) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 11, 17, 35, 54, 682, DateTimeKind.Local).AddTicks(1220), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/370.jpg", new DateTime(2021, 6, 11, 17, 35, 54, 682, DateTimeKind.Local).AddTicks(1224) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 11, 17, 35, 54, 682, DateTimeKind.Local).AddTicks(1238), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/90.jpg", new DateTime(2021, 6, 11, 17, 35, 54, 682, DateTimeKind.Local).AddTicks(1242) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 11, 17, 35, 54, 682, DateTimeKind.Local).AddTicks(1255), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/149.jpg", new DateTime(2021, 6, 11, 17, 35, 54, 682, DateTimeKind.Local).AddTicks(1259) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 11, 17, 35, 54, 682, DateTimeKind.Local).AddTicks(1272), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/1070.jpg", new DateTime(2021, 6, 11, 17, 35, 54, 682, DateTimeKind.Local).AddTicks(1276) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 11, 17, 35, 54, 682, DateTimeKind.Local).AddTicks(1290), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/1249.jpg", new DateTime(2021, 6, 11, 17, 35, 54, 682, DateTimeKind.Local).AddTicks(1293) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 11, 17, 35, 54, 682, DateTimeKind.Local).AddTicks(1306), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/233.jpg", new DateTime(2021, 6, 11, 17, 35, 54, 682, DateTimeKind.Local).AddTicks(1310) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 11, 17, 35, 54, 682, DateTimeKind.Local).AddTicks(1456), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/109.jpg", new DateTime(2021, 6, 11, 17, 35, 54, 682, DateTimeKind.Local).AddTicks(1460) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 11, 17, 35, 54, 682, DateTimeKind.Local).AddTicks(1473), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/781.jpg", new DateTime(2021, 6, 11, 17, 35, 54, 682, DateTimeKind.Local).AddTicks(1477) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 11, 17, 35, 54, 682, DateTimeKind.Local).AddTicks(1488), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/548.jpg", new DateTime(2021, 6, 11, 17, 35, 54, 682, DateTimeKind.Local).AddTicks(1492) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 11, 17, 35, 54, 682, DateTimeKind.Local).AddTicks(1504), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/675.jpg", new DateTime(2021, 6, 11, 17, 35, 54, 682, DateTimeKind.Local).AddTicks(1507) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 11, 17, 35, 54, 682, DateTimeKind.Local).AddTicks(1519), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/38.jpg", new DateTime(2021, 6, 11, 17, 35, 54, 682, DateTimeKind.Local).AddTicks(1522) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 11, 17, 35, 54, 682, DateTimeKind.Local).AddTicks(1534), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/671.jpg", new DateTime(2021, 6, 11, 17, 35, 54, 682, DateTimeKind.Local).AddTicks(1538) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 11, 17, 35, 54, 682, DateTimeKind.Local).AddTicks(1550), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/600.jpg", new DateTime(2021, 6, 11, 17, 35, 54, 682, DateTimeKind.Local).AddTicks(1553) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 11, 17, 35, 54, 682, DateTimeKind.Local).AddTicks(1565), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/933.jpg", new DateTime(2021, 6, 11, 17, 35, 54, 682, DateTimeKind.Local).AddTicks(1568) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 11, 17, 35, 54, 682, DateTimeKind.Local).AddTicks(1579), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/541.jpg", new DateTime(2021, 6, 11, 17, 35, 54, 682, DateTimeKind.Local).AddTicks(1583) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 11, 17, 35, 54, 686, DateTimeKind.Local).AddTicks(4621), "https://picsum.photos/640/480/?image=368", new DateTime(2021, 6, 11, 17, 35, 54, 686, DateTimeKind.Local).AddTicks(5326) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 22,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 11, 17, 35, 54, 686, DateTimeKind.Local).AddTicks(5499), "https://picsum.photos/640/480/?image=1066", new DateTime(2021, 6, 11, 17, 35, 54, 686, DateTimeKind.Local).AddTicks(5504) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 23,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 11, 17, 35, 54, 686, DateTimeKind.Local).AddTicks(5521), "https://picsum.photos/640/480/?image=354", new DateTime(2021, 6, 11, 17, 35, 54, 686, DateTimeKind.Local).AddTicks(5524) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 24,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 11, 17, 35, 54, 686, DateTimeKind.Local).AddTicks(5544), "https://picsum.photos/640/480/?image=328", new DateTime(2021, 6, 11, 17, 35, 54, 686, DateTimeKind.Local).AddTicks(5547) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 25,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 11, 17, 35, 54, 686, DateTimeKind.Local).AddTicks(5561), "https://picsum.photos/640/480/?image=867", new DateTime(2021, 6, 11, 17, 35, 54, 686, DateTimeKind.Local).AddTicks(5564) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 26,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 11, 17, 35, 54, 686, DateTimeKind.Local).AddTicks(5577), "https://picsum.photos/640/480/?image=751", new DateTime(2021, 6, 11, 17, 35, 54, 686, DateTimeKind.Local).AddTicks(5581) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 27,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 11, 17, 35, 54, 686, DateTimeKind.Local).AddTicks(5593), "https://picsum.photos/640/480/?image=496", new DateTime(2021, 6, 11, 17, 35, 54, 686, DateTimeKind.Local).AddTicks(5597) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 28,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 11, 17, 35, 54, 686, DateTimeKind.Local).AddTicks(5609), "https://picsum.photos/640/480/?image=498", new DateTime(2021, 6, 11, 17, 35, 54, 686, DateTimeKind.Local).AddTicks(5612) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 29,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 11, 17, 35, 54, 686, DateTimeKind.Local).AddTicks(5625), "https://picsum.photos/640/480/?image=926", new DateTime(2021, 6, 11, 17, 35, 54, 686, DateTimeKind.Local).AddTicks(5628) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 30,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 11, 17, 35, 54, 686, DateTimeKind.Local).AddTicks(5641), "https://picsum.photos/640/480/?image=402", new DateTime(2021, 6, 11, 17, 35, 54, 686, DateTimeKind.Local).AddTicks(5644) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 31,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 11, 17, 35, 54, 686, DateTimeKind.Local).AddTicks(5657), "https://picsum.photos/640/480/?image=1067", new DateTime(2021, 6, 11, 17, 35, 54, 686, DateTimeKind.Local).AddTicks(5660) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 32,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 11, 17, 35, 54, 686, DateTimeKind.Local).AddTicks(5672), "https://picsum.photos/640/480/?image=310", new DateTime(2021, 6, 11, 17, 35, 54, 686, DateTimeKind.Local).AddTicks(5675) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 33,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 11, 17, 35, 54, 686, DateTimeKind.Local).AddTicks(5688), "https://picsum.photos/640/480/?image=272", new DateTime(2021, 6, 11, 17, 35, 54, 686, DateTimeKind.Local).AddTicks(5691) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 34,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 11, 17, 35, 54, 686, DateTimeKind.Local).AddTicks(5704), "https://picsum.photos/640/480/?image=45", new DateTime(2021, 6, 11, 17, 35, 54, 686, DateTimeKind.Local).AddTicks(5707) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 35,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 11, 17, 35, 54, 686, DateTimeKind.Local).AddTicks(5719), "https://picsum.photos/640/480/?image=980", new DateTime(2021, 6, 11, 17, 35, 54, 686, DateTimeKind.Local).AddTicks(5723) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 36,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 11, 17, 35, 54, 686, DateTimeKind.Local).AddTicks(5735), "https://picsum.photos/640/480/?image=565", new DateTime(2021, 6, 11, 17, 35, 54, 686, DateTimeKind.Local).AddTicks(5738) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 37,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 11, 17, 35, 54, 686, DateTimeKind.Local).AddTicks(5755), "https://picsum.photos/640/480/?image=967", new DateTime(2021, 6, 11, 17, 35, 54, 686, DateTimeKind.Local).AddTicks(5758) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 38,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 11, 17, 35, 54, 686, DateTimeKind.Local).AddTicks(5770), "https://picsum.photos/640/480/?image=357", new DateTime(2021, 6, 11, 17, 35, 54, 686, DateTimeKind.Local).AddTicks(5774) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 39,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 11, 17, 35, 54, 686, DateTimeKind.Local).AddTicks(5787), "https://picsum.photos/640/480/?image=425", new DateTime(2021, 6, 11, 17, 35, 54, 686, DateTimeKind.Local).AddTicks(5790) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 40,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 11, 17, 35, 54, 686, DateTimeKind.Local).AddTicks(5802), "https://picsum.photos/640/480/?image=46", new DateTime(2021, 6, 11, 17, 35, 54, 686, DateTimeKind.Local).AddTicks(5805) });

            migrationBuilder.InsertData(
                table: "PostReactions",
                columns: new[] { "Id", "CreatedAt", "IsDisLike", "IsLike", "PostId", "UpdatedAt", "UserId" },
                values: new object[,]
                {
                    { 20, new DateTime(2021, 6, 11, 17, 35, 54, 998, DateTimeKind.Local).AddTicks(8469), true, true, 14, new DateTime(2021, 6, 11, 17, 35, 54, 998, DateTimeKind.Local).AddTicks(8473), 7 },
                    { 1, new DateTime(2021, 6, 11, 17, 35, 54, 998, DateTimeKind.Local).AddTicks(6823), false, false, 17, new DateTime(2021, 6, 11, 17, 35, 54, 998, DateTimeKind.Local).AddTicks(7489), 18 },
                    { 19, new DateTime(2021, 6, 11, 17, 35, 54, 998, DateTimeKind.Local).AddTicks(8443), true, false, 11, new DateTime(2021, 6, 11, 17, 35, 54, 998, DateTimeKind.Local).AddTicks(8446), 1 },
                    { 18, new DateTime(2021, 6, 11, 17, 35, 54, 998, DateTimeKind.Local).AddTicks(8421), true, false, 15, new DateTime(2021, 6, 11, 17, 35, 54, 998, DateTimeKind.Local).AddTicks(8424), 21 }
                });

            migrationBuilder.InsertData(
                table: "PostReactions",
                columns: new[] { "Id", "CreatedAt", "IsDisLike", "IsLike", "PostId", "UpdatedAt", "UserId" },
                values: new object[,]
                {
                    { 17, new DateTime(2021, 6, 11, 17, 35, 54, 998, DateTimeKind.Local).AddTicks(8394), false, false, 5, new DateTime(2021, 6, 11, 17, 35, 54, 998, DateTimeKind.Local).AddTicks(8397), 7 },
                    { 16, new DateTime(2021, 6, 11, 17, 35, 54, 998, DateTimeKind.Local).AddTicks(8373), false, false, 9, new DateTime(2021, 6, 11, 17, 35, 54, 998, DateTimeKind.Local).AddTicks(8377), 20 },
                    { 14, new DateTime(2021, 6, 11, 17, 35, 54, 998, DateTimeKind.Local).AddTicks(8330), false, false, 13, new DateTime(2021, 6, 11, 17, 35, 54, 998, DateTimeKind.Local).AddTicks(8334), 16 },
                    { 13, new DateTime(2021, 6, 11, 17, 35, 54, 998, DateTimeKind.Local).AddTicks(8309), false, false, 7, new DateTime(2021, 6, 11, 17, 35, 54, 998, DateTimeKind.Local).AddTicks(8313), 2 },
                    { 12, new DateTime(2021, 6, 11, 17, 35, 54, 998, DateTimeKind.Local).AddTicks(8286), true, true, 11, new DateTime(2021, 6, 11, 17, 35, 54, 998, DateTimeKind.Local).AddTicks(8290), 16 },
                    { 11, new DateTime(2021, 6, 11, 17, 35, 54, 998, DateTimeKind.Local).AddTicks(8265), false, true, 2, new DateTime(2021, 6, 11, 17, 35, 54, 998, DateTimeKind.Local).AddTicks(8268), 9 },
                    { 15, new DateTime(2021, 6, 11, 17, 35, 54, 998, DateTimeKind.Local).AddTicks(8352), true, false, 9, new DateTime(2021, 6, 11, 17, 35, 54, 998, DateTimeKind.Local).AddTicks(8355), 18 },
                    { 9, new DateTime(2021, 6, 11, 17, 35, 54, 998, DateTimeKind.Local).AddTicks(8216), false, false, 18, new DateTime(2021, 6, 11, 17, 35, 54, 998, DateTimeKind.Local).AddTicks(8219), 18 },
                    { 8, new DateTime(2021, 6, 11, 17, 35, 54, 998, DateTimeKind.Local).AddTicks(8194), true, true, 13, new DateTime(2021, 6, 11, 17, 35, 54, 998, DateTimeKind.Local).AddTicks(8198), 7 },
                    { 7, new DateTime(2021, 6, 11, 17, 35, 54, 998, DateTimeKind.Local).AddTicks(8170), false, true, 5, new DateTime(2021, 6, 11, 17, 35, 54, 998, DateTimeKind.Local).AddTicks(8174), 12 },
                    { 6, new DateTime(2021, 6, 11, 17, 35, 54, 998, DateTimeKind.Local).AddTicks(8077), false, true, 9, new DateTime(2021, 6, 11, 17, 35, 54, 998, DateTimeKind.Local).AddTicks(8081), 4 },
                    { 10, new DateTime(2021, 6, 11, 17, 35, 54, 998, DateTimeKind.Local).AddTicks(8242), false, false, 8, new DateTime(2021, 6, 11, 17, 35, 54, 998, DateTimeKind.Local).AddTicks(8245), 1 },
                    { 5, new DateTime(2021, 6, 11, 17, 35, 54, 998, DateTimeKind.Local).AddTicks(8055), false, true, 7, new DateTime(2021, 6, 11, 17, 35, 54, 998, DateTimeKind.Local).AddTicks(8058), 12 },
                    { 4, new DateTime(2021, 6, 11, 17, 35, 54, 998, DateTimeKind.Local).AddTicks(8031), true, true, 12, new DateTime(2021, 6, 11, 17, 35, 54, 998, DateTimeKind.Local).AddTicks(8035), 6 },
                    { 3, new DateTime(2021, 6, 11, 17, 35, 54, 998, DateTimeKind.Local).AddTicks(8007), true, false, 5, new DateTime(2021, 6, 11, 17, 35, 54, 998, DateTimeKind.Local).AddTicks(8011), 4 },
                    { 2, new DateTime(2021, 6, 11, 17, 35, 54, 998, DateTimeKind.Local).AddTicks(7972), true, true, 20, new DateTime(2021, 6, 11, 17, 35, 54, 998, DateTimeKind.Local).AddTicks(7979), 16 }
                });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 3, "Dolore ullam sunt placeat quidem blanditiis debitis molestias doloribus dolor.\nNam odio delectus sunt possimus aut quis deserunt maiores et.\nIllum aut quia architecto consequatur facilis corporis voluptatem quis.\nFacilis aut corrupti illo fugit iure aut non.\nQuia qui fugit qui eveniet aut.", new DateTime(2021, 6, 11, 17, 35, 54, 977, DateTimeKind.Local).AddTicks(6873), 36, new DateTime(2021, 6, 11, 17, 35, 54, 977, DateTimeKind.Local).AddTicks(7900) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 10, "Quae minus soluta velit atque. In error ducimus. Quia quo dolores veniam.", new DateTime(2021, 6, 11, 17, 35, 54, 978, DateTimeKind.Local).AddTicks(988), 21, new DateTime(2021, 6, 11, 17, 35, 54, 978, DateTimeKind.Local).AddTicks(1014) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 12, "Facilis est quos.", new DateTime(2021, 6, 11, 17, 35, 54, 978, DateTimeKind.Local).AddTicks(2156), 34, new DateTime(2021, 6, 11, 17, 35, 54, 978, DateTimeKind.Local).AddTicks(2179) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 9, "perferendis", new DateTime(2021, 6, 11, 17, 35, 54, 978, DateTimeKind.Local).AddTicks(2828), 35, new DateTime(2021, 6, 11, 17, 35, 54, 978, DateTimeKind.Local).AddTicks(2855) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 20, "Non occaecati quos perferendis rerum.", new DateTime(2021, 6, 11, 17, 35, 54, 978, DateTimeKind.Local).AddTicks(2988), 32, new DateTime(2021, 6, 11, 17, 35, 54, 978, DateTimeKind.Local).AddTicks(2996) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 11, "Similique doloremque vel.", new DateTime(2021, 6, 11, 17, 35, 54, 978, DateTimeKind.Local).AddTicks(3071), 27, new DateTime(2021, 6, 11, 17, 35, 54, 978, DateTimeKind.Local).AddTicks(3078) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 14, "ut", new DateTime(2021, 6, 11, 17, 35, 54, 978, DateTimeKind.Local).AddTicks(3142), 40, new DateTime(2021, 6, 11, 17, 35, 54, 978, DateTimeKind.Local).AddTicks(3148) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 14, "Autem quia sunt dolorem esse pariatur eum amet laborum in.", new DateTime(2021, 6, 11, 17, 35, 54, 978, DateTimeKind.Local).AddTicks(3366), 36, new DateTime(2021, 6, 11, 17, 35, 54, 978, DateTimeKind.Local).AddTicks(3375) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 3, "Labore quo quo dolorem unde.", new DateTime(2021, 6, 11, 17, 35, 54, 978, DateTimeKind.Local).AddTicks(3469), 34, new DateTime(2021, 6, 11, 17, 35, 54, 978, DateTimeKind.Local).AddTicks(3486) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 13, "in", new DateTime(2021, 6, 11, 17, 35, 54, 978, DateTimeKind.Local).AddTicks(3591), 30, new DateTime(2021, 6, 11, 17, 35, 54, 978, DateTimeKind.Local).AddTicks(3598) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 17, "Nisi aspernatur doloremque vitae. In saepe rerum molestiae. Mollitia explicabo numquam est porro.", new DateTime(2021, 6, 11, 17, 35, 54, 978, DateTimeKind.Local).AddTicks(3781), 34, new DateTime(2021, 6, 11, 17, 35, 54, 978, DateTimeKind.Local).AddTicks(3793) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 6, "Voluptatem labore dolores quaerat sequi quasi laborum.", new DateTime(2021, 6, 11, 17, 35, 54, 978, DateTimeKind.Local).AddTicks(3886), 36, new DateTime(2021, 6, 11, 17, 35, 54, 978, DateTimeKind.Local).AddTicks(3892) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 17, "Quas cum adipisci nulla sint consequatur cumque. Eveniet ea aliquid veniam reprehenderit incidunt qui. Eum iure est soluta ut qui. Rerum nulla voluptas debitis. Sint harum dolorum.", new DateTime(2021, 6, 11, 17, 35, 54, 978, DateTimeKind.Local).AddTicks(4230), 40, new DateTime(2021, 6, 11, 17, 35, 54, 978, DateTimeKind.Local).AddTicks(4240) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 21, "Sit aut velit nemo dicta nulla. Et necessitatibus numquam accusamus molestiae dolorum eos. Necessitatibus velit earum voluptatem odit occaecati.", new DateTime(2021, 6, 11, 17, 35, 54, 978, DateTimeKind.Local).AddTicks(4512), 26, new DateTime(2021, 6, 11, 17, 35, 54, 978, DateTimeKind.Local).AddTicks(4532) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 9, "Perferendis ut repellendus iure voluptatum fuga eos doloremque.\nQuas amet esse deserunt reprehenderit dolor.\nInventore harum aut hic reprehenderit autem magnam.\nVoluptas eos ducimus maiores ut assumenda aut cupiditate.", new DateTime(2021, 6, 11, 17, 35, 54, 978, DateTimeKind.Local).AddTicks(4845), 37, new DateTime(2021, 6, 11, 17, 35, 54, 978, DateTimeKind.Local).AddTicks(4853) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 5, "Et id dolore temporibus officia nobis debitis nihil dolor. Non voluptas voluptatibus adipisci consequatur. Perspiciatis ullam quidem sit illo dolorum laborum quia. Magni nobis fuga distinctio.", new DateTime(2021, 6, 11, 17, 35, 54, 978, DateTimeKind.Local).AddTicks(5444), 33, new DateTime(2021, 6, 11, 17, 35, 54, 978, DateTimeKind.Local).AddTicks(5465) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 6, "Iste saepe qui nesciunt aut eligendi quis accusantium.\nExcepturi occaecati expedita sint in sint dolorem enim aspernatur velit.\nOmnis debitis consequuntur quasi quidem.\nRepellat aliquam ipsa dolor sequi quis.", new DateTime(2021, 6, 11, 17, 35, 54, 978, DateTimeKind.Local).AddTicks(5795), 38, new DateTime(2021, 6, 11, 17, 35, 54, 978, DateTimeKind.Local).AddTicks(5805) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 4, "Eum quos earum facilis accusantium quidem aspernatur expedita dolores numquam.\nOdit quo sit.\nEa aspernatur non.\nAssumenda expedita cupiditate ut eligendi sed voluptatibus aut.", new DateTime(2021, 6, 11, 17, 35, 54, 978, DateTimeKind.Local).AddTicks(6105), 28, new DateTime(2021, 6, 11, 17, 35, 54, 978, DateTimeKind.Local).AddTicks(6121) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 10, "Ipsa pariatur rerum id aliquam est placeat et accusantium velit. Et necessitatibus ratione quia. Inventore perferendis quibusdam ut a nihil ipsam eos ratione. Non ipsa fugiat quod excepturi. Sunt ipsum veritatis maiores dignissimos quos aut voluptate.", new DateTime(2021, 6, 11, 17, 35, 54, 978, DateTimeKind.Local).AddTicks(6421), 24, new DateTime(2021, 6, 11, 17, 35, 54, 978, DateTimeKind.Local).AddTicks(6433) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 9, "Odit et ut omnis tenetur ullam.\nSapiente nostrum voluptatibus laboriosam.\nAccusantium praesentium alias neque nihil voluptate quis.", new DateTime(2021, 6, 11, 17, 35, 54, 978, DateTimeKind.Local).AddTicks(6685), 39, new DateTime(2021, 6, 11, 17, 35, 54, 978, DateTimeKind.Local).AddTicks(6702) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 17, new DateTime(2021, 6, 11, 17, 35, 54, 739, DateTimeKind.Local).AddTicks(8148), "Carey_Jones5@gmail.com", "0hxhWX9XdFW5x9fm8g4Y6FK7S5XxcRGXQ791CY+Dx34=", "6yUZ45S+DajSKizYW1cwfedfw9K/Pyp63rwTLCgV0W8=", new DateTime(2021, 6, 11, 17, 35, 54, 739, DateTimeKind.Local).AddTicks(9374), "Rita27" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 16, new DateTime(2021, 6, 11, 17, 35, 54, 749, DateTimeKind.Local).AddTicks(9976), "Ashly.Purdy@gmail.com", "tUMZ84j0xTktiD7JOrU1Y17Y0Adb3vmovo8mj3mp/ds=", "rSDxsLY23o5Wvx/ln8y0RMrEtJOdZ2RuVFfo3UirZPE=", new DateTime(2021, 6, 11, 17, 35, 54, 750, DateTimeKind.Local).AddTicks(34), "Kristy13" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 13, new DateTime(2021, 6, 11, 17, 35, 54, 761, DateTimeKind.Local).AddTicks(7638), "Ivy.VonRueden72@hotmail.com", "AeW5oNPk9S6TMPJesvc+88lXUp0erfMjR0JV9YHe3ws=", "/GRV1xNAXXEhgxjjpWLQl/et+zxValQnnAt5MNmI7sM=", new DateTime(2021, 6, 11, 17, 35, 54, 761, DateTimeKind.Local).AddTicks(7703), "Rosemary31" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 18, new DateTime(2021, 6, 11, 17, 35, 54, 771, DateTimeKind.Local).AddTicks(4360), "Jett_Homenick@yahoo.com", "HSqgBQV926DROhQaYumGvAJEnGTGOzW7kWU6zQZ44MU=", "tc0GmcQXY7DLsuJTJ7p179uBGjnbf1fbNVOtw8f2Qak=", new DateTime(2021, 6, 11, 17, 35, 54, 771, DateTimeKind.Local).AddTicks(4424), "Gabriel73" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 6, new DateTime(2021, 6, 11, 17, 35, 54, 781, DateTimeKind.Local).AddTicks(5913), "Jade.Bruen@yahoo.com", "7Ef8SOGeLH0nscmhl5kgHbOdyMkWMLxnjYijkQ3w64A=", "QTUZUF6+RdL+E+O0rDehoFotNhnAnWzGM7a7PmbXgrY=", new DateTime(2021, 6, 11, 17, 35, 54, 781, DateTimeKind.Local).AddTicks(5969), "Justen80" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 13, new DateTime(2021, 6, 11, 17, 35, 54, 793, DateTimeKind.Local).AddTicks(1909), "Deondre.Funk@yahoo.com", "WmQ2V3DUqGxtNDwGkItXbZfTws42lGVRX3fs30HPTC4=", "ZdQaCMCUjWwMDeVyiRzCNNb5BLe7YqRQutPN2OBOrp4=", new DateTime(2021, 6, 11, 17, 35, 54, 793, DateTimeKind.Local).AddTicks(1975), "Oswald_Fahey" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 18, new DateTime(2021, 6, 11, 17, 35, 54, 804, DateTimeKind.Local).AddTicks(2576), "Claire.Smith40@gmail.com", "7x6CtrTsSjpULl0WJEEMvLHWBvmR7bvjXMj9aM720qs=", "kVcOibsDmXkNCAOgCFryt6FrQTPNirtL37OP83kDKNw=", new DateTime(2021, 6, 11, 17, 35, 54, 804, DateTimeKind.Local).AddTicks(2643), "Jean_Nicolas3" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 11, new DateTime(2021, 6, 11, 17, 35, 54, 813, DateTimeKind.Local).AddTicks(705), "Sandrine_Harris36@yahoo.com", "0fN2IJHC9UthdYs8WFIyc+DfMTedmNvZ5d5hPSc57Ks=", "xLZRvvm8twtp1i2iLaIURbK74ja9XZJ5P/yu7ZOHd2w=", new DateTime(2021, 6, 11, 17, 35, 54, 813, DateTimeKind.Local).AddTicks(778), "Audie61" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 20, new DateTime(2021, 6, 11, 17, 35, 54, 824, DateTimeKind.Local).AddTicks(4928), "Van.Block@yahoo.com", "yXEZT4Tg3qdquB2/CbaipPu3TJGMC6FY1+fDXKuibfc=", "sbTlMSx0IHH2FaaOzO0LgnIv5qTR0FxSrkITCTBjGXk=", new DateTime(2021, 6, 11, 17, 35, 54, 824, DateTimeKind.Local).AddTicks(4996), "Spencer.DuBuque" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 19, new DateTime(2021, 6, 11, 17, 35, 54, 836, DateTimeKind.Local).AddTicks(2575), "Agustina81@yahoo.com", "zy2fB8e2/WjOvaTxd4WNzBj7sCkCPliPEKVyYq61UUc=", "urA/SUoNinwfDzE50fnrXoUEry6Cj//7vC0shj1LrHw=", new DateTime(2021, 6, 11, 17, 35, 54, 836, DateTimeKind.Local).AddTicks(2639), "Eryn_Kertzmann90" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 7, new DateTime(2021, 6, 11, 17, 35, 54, 846, DateTimeKind.Local).AddTicks(9122), "Anais_White1@gmail.com", "I6dfHbJTKjVQY5zo8wmNHv8awvAgnm+0A+ujBadwYWI=", "YZByMGqY2SiZB/Qi1ZUJierYhtfMDAvL0MrCbk+lB0M=", new DateTime(2021, 6, 11, 17, 35, 54, 846, DateTimeKind.Local).AddTicks(9184), "Myrtice.Howe64" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 8, new DateTime(2021, 6, 11, 17, 35, 54, 857, DateTimeKind.Local).AddTicks(5940), "Phyllis_Koss26@gmail.com", "IWu+n4Zf8Ni7X6oSTOrl2WKPE0uc4dI6IyVK30J3Zcw=", "cg1XQelQCfc79vRdzOpos7zOvFtxjNGxWVHCmcUORq4=", new DateTime(2021, 6, 11, 17, 35, 54, 857, DateTimeKind.Local).AddTicks(6043), "Jermey42" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 6, new DateTime(2021, 6, 11, 17, 35, 54, 868, DateTimeKind.Local).AddTicks(6369), "Mellie35@gmail.com", "tWBUx8E8v0kHX97zeqWzegv8WrbD+UDxB+F9YFvR0kw=", "VNkE7SPZ3/hhruaVEQyRtxpuI/2KVziF5c4B6R6pVOM=", new DateTime(2021, 6, 11, 17, 35, 54, 868, DateTimeKind.Local).AddTicks(6443), "Birdie_Hane40" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 1, new DateTime(2021, 6, 11, 17, 35, 54, 879, DateTimeKind.Local).AddTicks(6080), "Zachery.Schneider@gmail.com", "ax5pAgyuTg4+09iCxyCA3T/EX/00m2qgae5gZIJr4X0=", "IdSq+/labFFs8od50wsyybVJMCCR5gEiqDwOowAWcp4=", new DateTime(2021, 6, 11, 17, 35, 54, 879, DateTimeKind.Local).AddTicks(6146), "Kathryne_Bode" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 15, new DateTime(2021, 6, 11, 17, 35, 54, 890, DateTimeKind.Local).AddTicks(5734), "Jessyca_Goodwin@yahoo.com", "RoDYT/tn8eHNXsk7hVDM3541RiDX+mEqmr/Mi/e3Xhw=", "UUkdWqYxZj1gYeq6M8sgW4oJOAU7BrepTaCGCYGp4mo=", new DateTime(2021, 6, 11, 17, 35, 54, 890, DateTimeKind.Local).AddTicks(5794), "Maurice_Breitenberg17" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 18, new DateTime(2021, 6, 11, 17, 35, 54, 902, DateTimeKind.Local).AddTicks(5882), "Nicola_Nikolaus@gmail.com", "0U7YWA3nhAGcTNK9YMNYKkkZurnp3iE+OE7JSjK6Qgs=", "Ckcs18cj7qHfKT0TaifvFXlvF7iOj/RYhkoOxNBO1dM=", new DateTime(2021, 6, 11, 17, 35, 54, 902, DateTimeKind.Local).AddTicks(5946), "Alfreda.Abshire57" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 11, new DateTime(2021, 6, 11, 17, 35, 54, 914, DateTimeKind.Local).AddTicks(3500), "Domenico39@yahoo.com", "XpW8jA8ETddD+60Dq93EG3XMY/wnlXrmGYjDTRUraPE=", "1E+3kwimvfbmJf7X/tF4TEpa8Y8xvg868XdqYhsml7o=", new DateTime(2021, 6, 11, 17, 35, 54, 914, DateTimeKind.Local).AddTicks(3557), "Nelda_Jerde88" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 18, new DateTime(2021, 6, 11, 17, 35, 54, 926, DateTimeKind.Local).AddTicks(4551), "Pasquale_Murray@yahoo.com", "/Vb+TjuEZDe28Ax+srNjsfYRbzMyWgVJHcPM/2DmKhA=", "MEeZNcB+g8JRl7lQ1nP1Jg/KuHpKy17Dn1xjVgRzkBk=", new DateTime(2021, 6, 11, 17, 35, 54, 926, DateTimeKind.Local).AddTicks(4615), "Greg_Turcotte" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 18, new DateTime(2021, 6, 11, 17, 35, 54, 938, DateTimeKind.Local).AddTicks(652), "Nannie_Homenick@gmail.com", "yBVgVg+qknmOfStkmOJ/SzQfZgUPKvYcT7UECplOdIk=", "DRfSsYCyplfbPjvBYXqc83vknyais2DlVhzBD2vQX2w=", new DateTime(2021, 6, 11, 17, 35, 54, 938, DateTimeKind.Local).AddTicks(722), "Adrienne_Monahan" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 15, new DateTime(2021, 6, 11, 17, 35, 54, 951, DateTimeKind.Local).AddTicks(2504), "Myriam13@hotmail.com", "hPP7eVDOEwXPh1SH1tPsE6gW8G+S0S3ReeuSy0TaSl0=", "Izi6McMQ1KP5WBUaWndfjxWxcx3rAJgQadpJBxT4sZk=", new DateTime(2021, 6, 11, 17, 35, 54, 951, DateTimeKind.Local).AddTicks(2579), "Grover36" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "CreatedAt", "Password", "Salt", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 11, 17, 35, 54, 961, DateTimeKind.Local).AddTicks(853), "EBGqpLyLcW3etdPPBwDpXAXB1u4u2ugmWLWcnjvcXeQ=", "EDz/z3WKKIQEV/GIUylnQZT1EmSdj4+xnON6fbdEjnQ=", new DateTime(2021, 6, 11, 17, 35, 54, 961, DateTimeKind.Local).AddTicks(853) });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 20);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 20);

            migrationBuilder.DropColumn(
                name: "IsDisLike",
                table: "PostReactions");

            migrationBuilder.DropColumn(
                name: "IsDisLike",
                table: "CommentReactions");

            migrationBuilder.InsertData(
                table: "CommentReactions",
                columns: new[] { "Id", "CommentId", "CreatedAt", "IsLike", "UpdatedAt", "UserId" },
                values: new object[,]
                {
                    { 1, 4, new DateTime(2021, 5, 20, 11, 13, 15, 61, DateTimeKind.Local).AddTicks(3131), true, new DateTime(2021, 5, 20, 11, 13, 15, 61, DateTimeKind.Local).AddTicks(4182), 8 },
                    { 18, 15, new DateTime(2021, 5, 20, 11, 13, 15, 61, DateTimeKind.Local).AddTicks(5568), true, new DateTime(2021, 5, 20, 11, 13, 15, 61, DateTimeKind.Local).AddTicks(5573), 3 },
                    { 17, 20, new DateTime(2021, 5, 20, 11, 13, 15, 61, DateTimeKind.Local).AddTicks(5536), false, new DateTime(2021, 5, 20, 11, 13, 15, 61, DateTimeKind.Local).AddTicks(5541), 16 },
                    { 15, 13, new DateTime(2021, 5, 20, 11, 13, 15, 61, DateTimeKind.Local).AddTicks(5470), false, new DateTime(2021, 5, 20, 11, 13, 15, 61, DateTimeKind.Local).AddTicks(5476), 14 },
                    { 14, 6, new DateTime(2021, 5, 20, 11, 13, 15, 61, DateTimeKind.Local).AddTicks(5438), false, new DateTime(2021, 5, 20, 11, 13, 15, 61, DateTimeKind.Local).AddTicks(5443), 10 },
                    { 13, 18, new DateTime(2021, 5, 20, 11, 13, 15, 61, DateTimeKind.Local).AddTicks(5405), false, new DateTime(2021, 5, 20, 11, 13, 15, 61, DateTimeKind.Local).AddTicks(5411), 3 },
                    { 12, 2, new DateTime(2021, 5, 20, 11, 13, 15, 61, DateTimeKind.Local).AddTicks(5372), false, new DateTime(2021, 5, 20, 11, 13, 15, 61, DateTimeKind.Local).AddTicks(5378), 1 },
                    { 11, 12, new DateTime(2021, 5, 20, 11, 13, 15, 61, DateTimeKind.Local).AddTicks(5340), false, new DateTime(2021, 5, 20, 11, 13, 15, 61, DateTimeKind.Local).AddTicks(5345), 13 },
                    { 19, 16, new DateTime(2021, 5, 20, 11, 13, 15, 61, DateTimeKind.Local).AddTicks(5599), true, new DateTime(2021, 5, 20, 11, 13, 15, 61, DateTimeKind.Local).AddTicks(5605), 21 },
                    { 10, 16, new DateTime(2021, 5, 20, 11, 13, 15, 61, DateTimeKind.Local).AddTicks(5307), false, new DateTime(2021, 5, 20, 11, 13, 15, 61, DateTimeKind.Local).AddTicks(5313), 1 },
                    { 8, 14, new DateTime(2021, 5, 20, 11, 13, 15, 61, DateTimeKind.Local).AddTicks(5242), true, new DateTime(2021, 5, 20, 11, 13, 15, 61, DateTimeKind.Local).AddTicks(5247), 13 },
                    { 7, 6, new DateTime(2021, 5, 20, 11, 13, 15, 61, DateTimeKind.Local).AddTicks(5209), false, new DateTime(2021, 5, 20, 11, 13, 15, 61, DateTimeKind.Local).AddTicks(5215), 17 },
                    { 6, 4, new DateTime(2021, 5, 20, 11, 13, 15, 61, DateTimeKind.Local).AddTicks(5176), false, new DateTime(2021, 5, 20, 11, 13, 15, 61, DateTimeKind.Local).AddTicks(5182), 7 },
                    { 5, 19, new DateTime(2021, 5, 20, 11, 13, 15, 61, DateTimeKind.Local).AddTicks(5131), false, new DateTime(2021, 5, 20, 11, 13, 15, 61, DateTimeKind.Local).AddTicks(5139), 21 },
                    { 4, 13, new DateTime(2021, 5, 20, 11, 13, 15, 61, DateTimeKind.Local).AddTicks(4985), false, new DateTime(2021, 5, 20, 11, 13, 15, 61, DateTimeKind.Local).AddTicks(4996), 12 },
                    { 3, 6, new DateTime(2021, 5, 20, 11, 13, 15, 61, DateTimeKind.Local).AddTicks(4924), true, new DateTime(2021, 5, 20, 11, 13, 15, 61, DateTimeKind.Local).AddTicks(4934), 6 },
                    { 2, 11, new DateTime(2021, 5, 20, 11, 13, 15, 61, DateTimeKind.Local).AddTicks(4838), false, new DateTime(2021, 5, 20, 11, 13, 15, 61, DateTimeKind.Local).AddTicks(4856), 1 },
                    { 9, 13, new DateTime(2021, 5, 20, 11, 13, 15, 61, DateTimeKind.Local).AddTicks(5274), false, new DateTime(2021, 5, 20, 11, 13, 15, 61, DateTimeKind.Local).AddTicks(5280), 20 },
                    { 20, 2, new DateTime(2021, 5, 20, 11, 13, 15, 61, DateTimeKind.Local).AddTicks(5630), true, new DateTime(2021, 5, 20, 11, 13, 15, 61, DateTimeKind.Local).AddTicks(5635), 17 },
                    { 16, 3, new DateTime(2021, 5, 20, 11, 13, 15, 61, DateTimeKind.Local).AddTicks(5502), true, new DateTime(2021, 5, 20, 11, 13, 15, 61, DateTimeKind.Local).AddTicks(5508), 21 }
                });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 17, "Quo sunt necessitatibus impedit nobis repellendus voluptatem.", new DateTime(2021, 5, 20, 11, 13, 15, 45, DateTimeKind.Local).AddTicks(5039), 6, new DateTime(2021, 5, 20, 11, 13, 15, 45, DateTimeKind.Local).AddTicks(6199) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 20, "Mollitia et exercitationem aliquam animi iusto est cupiditate.", new DateTime(2021, 5, 20, 11, 13, 15, 45, DateTimeKind.Local).AddTicks(6984), 13, new DateTime(2021, 5, 20, 11, 13, 15, 45, DateTimeKind.Local).AddTicks(6997) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 16, "Porro nobis error.", new DateTime(2021, 5, 20, 11, 13, 15, 45, DateTimeKind.Local).AddTicks(7091), 14, new DateTime(2021, 5, 20, 11, 13, 15, 45, DateTimeKind.Local).AddTicks(7098) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 6, "A aliquid quia distinctio enim dolorem.", new DateTime(2021, 5, 20, 11, 13, 15, 45, DateTimeKind.Local).AddTicks(7193), 3, new DateTime(2021, 5, 20, 11, 13, 15, 45, DateTimeKind.Local).AddTicks(7200) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 3, "Sit ut harum minus atque quidem molestias sint consectetur.", new DateTime(2021, 5, 20, 11, 13, 15, 45, DateTimeKind.Local).AddTicks(7306), 5, new DateTime(2021, 5, 20, 11, 13, 15, 45, DateTimeKind.Local).AddTicks(7314) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 2, "Sapiente inventore nostrum.", new DateTime(2021, 5, 20, 11, 13, 15, 45, DateTimeKind.Local).AddTicks(7383), 5, new DateTime(2021, 5, 20, 11, 13, 15, 45, DateTimeKind.Local).AddTicks(7390) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 21, "Blanditiis sed ea quia.", new DateTime(2021, 5, 20, 11, 13, 15, 45, DateTimeKind.Local).AddTicks(7628), 20, new DateTime(2021, 5, 20, 11, 13, 15, 45, DateTimeKind.Local).AddTicks(7639) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 2, "Et dignissimos ut eaque voluptas labore.", new DateTime(2021, 5, 20, 11, 13, 15, 45, DateTimeKind.Local).AddTicks(7731), 3, new DateTime(2021, 5, 20, 11, 13, 15, 45, DateTimeKind.Local).AddTicks(7739) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 14, "Aliquam aut saepe ipsum consequuntur totam quia ipsum autem.", new DateTime(2021, 5, 20, 11, 13, 15, 45, DateTimeKind.Local).AddTicks(7842), 10, new DateTime(2021, 5, 20, 11, 13, 15, 45, DateTimeKind.Local).AddTicks(7850) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 15, "Asperiores nostrum doloremque animi et.", new DateTime(2021, 5, 20, 11, 13, 15, 45, DateTimeKind.Local).AddTicks(7989), 16, new DateTime(2021, 5, 20, 11, 13, 15, 45, DateTimeKind.Local).AddTicks(7996) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 14, "Esse vero officia.", new DateTime(2021, 5, 20, 11, 13, 15, 45, DateTimeKind.Local).AddTicks(8068), 7, new DateTime(2021, 5, 20, 11, 13, 15, 45, DateTimeKind.Local).AddTicks(8076) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 21, "Adipisci ut voluptatum eos accusamus dolor.", new DateTime(2021, 5, 20, 11, 13, 15, 45, DateTimeKind.Local).AddTicks(8164), 4, new DateTime(2021, 5, 20, 11, 13, 15, 45, DateTimeKind.Local).AddTicks(8172) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 12, "Sit dolorem aperiam alias maiores voluptates ut hic enim.", new DateTime(2021, 5, 20, 11, 13, 15, 45, DateTimeKind.Local).AddTicks(8356), 3, new DateTime(2021, 5, 20, 11, 13, 15, 45, DateTimeKind.Local).AddTicks(8366) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 6, "Voluptatem itaque iusto.", new DateTime(2021, 5, 20, 11, 13, 15, 45, DateTimeKind.Local).AddTicks(8438), 11, new DateTime(2021, 5, 20, 11, 13, 15, 45, DateTimeKind.Local).AddTicks(8445) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 18, "Consequuntur ea qui cupiditate quaerat.", new DateTime(2021, 5, 20, 11, 13, 15, 45, DateTimeKind.Local).AddTicks(8528), 15, new DateTime(2021, 5, 20, 11, 13, 15, 45, DateTimeKind.Local).AddTicks(8536) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 7, "Libero eligendi excepturi est veniam.", new DateTime(2021, 5, 20, 11, 13, 15, 45, DateTimeKind.Local).AddTicks(8622), 17, new DateTime(2021, 5, 20, 11, 13, 15, 45, DateTimeKind.Local).AddTicks(8629) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 4, "Pariatur et aperiam eligendi et explicabo.", new DateTime(2021, 5, 20, 11, 13, 15, 45, DateTimeKind.Local).AddTicks(8713), 19, new DateTime(2021, 5, 20, 11, 13, 15, 45, DateTimeKind.Local).AddTicks(8720) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 1, "Aut sed nisi ipsa animi.", new DateTime(2021, 5, 20, 11, 13, 15, 45, DateTimeKind.Local).AddTicks(8855), 13, new DateTime(2021, 5, 20, 11, 13, 15, 45, DateTimeKind.Local).AddTicks(8863) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 20, "Repudiandae pariatur eum voluptate expedita.", new DateTime(2021, 5, 20, 11, 13, 15, 45, DateTimeKind.Local).AddTicks(8949), 4, new DateTime(2021, 5, 20, 11, 13, 15, 45, DateTimeKind.Local).AddTicks(8957) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 6, "Quo et ut odio ut.", new DateTime(2021, 5, 20, 11, 13, 15, 45, DateTimeKind.Local).AddTicks(9135), 17, new DateTime(2021, 5, 20, 11, 13, 15, 45, DateTimeKind.Local).AddTicks(9144) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 5, 20, 11, 13, 14, 770, DateTimeKind.Local).AddTicks(1845), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/1081.jpg", new DateTime(2021, 5, 20, 11, 13, 14, 770, DateTimeKind.Local).AddTicks(7597) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 5, 20, 11, 13, 14, 770, DateTimeKind.Local).AddTicks(8310), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/462.jpg", new DateTime(2021, 5, 20, 11, 13, 14, 770, DateTimeKind.Local).AddTicks(8321) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 5, 20, 11, 13, 14, 770, DateTimeKind.Local).AddTicks(8352), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/1199.jpg", new DateTime(2021, 5, 20, 11, 13, 14, 770, DateTimeKind.Local).AddTicks(8357) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 5, 20, 11, 13, 14, 770, DateTimeKind.Local).AddTicks(8377), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/36.jpg", new DateTime(2021, 5, 20, 11, 13, 14, 770, DateTimeKind.Local).AddTicks(8382) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 5, 20, 11, 13, 14, 770, DateTimeKind.Local).AddTicks(8401), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/488.jpg", new DateTime(2021, 5, 20, 11, 13, 14, 770, DateTimeKind.Local).AddTicks(8406) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 5, 20, 11, 13, 14, 770, DateTimeKind.Local).AddTicks(8497), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/210.jpg", new DateTime(2021, 5, 20, 11, 13, 14, 770, DateTimeKind.Local).AddTicks(8504) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 5, 20, 11, 13, 14, 770, DateTimeKind.Local).AddTicks(8526), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/1032.jpg", new DateTime(2021, 5, 20, 11, 13, 14, 770, DateTimeKind.Local).AddTicks(8531) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 5, 20, 11, 13, 14, 770, DateTimeKind.Local).AddTicks(8551), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/4.jpg", new DateTime(2021, 5, 20, 11, 13, 14, 770, DateTimeKind.Local).AddTicks(8556) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 5, 20, 11, 13, 14, 770, DateTimeKind.Local).AddTicks(8574), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/1002.jpg", new DateTime(2021, 5, 20, 11, 13, 14, 770, DateTimeKind.Local).AddTicks(8579) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 5, 20, 11, 13, 14, 770, DateTimeKind.Local).AddTicks(8598), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/298.jpg", new DateTime(2021, 5, 20, 11, 13, 14, 770, DateTimeKind.Local).AddTicks(8603) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 5, 20, 11, 13, 14, 770, DateTimeKind.Local).AddTicks(8621), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/60.jpg", new DateTime(2021, 5, 20, 11, 13, 14, 770, DateTimeKind.Local).AddTicks(8626) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 5, 20, 11, 13, 14, 770, DateTimeKind.Local).AddTicks(8644), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/996.jpg", new DateTime(2021, 5, 20, 11, 13, 14, 770, DateTimeKind.Local).AddTicks(8650) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 5, 20, 11, 13, 14, 770, DateTimeKind.Local).AddTicks(8669), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/1043.jpg", new DateTime(2021, 5, 20, 11, 13, 14, 770, DateTimeKind.Local).AddTicks(8674) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 5, 20, 11, 13, 14, 770, DateTimeKind.Local).AddTicks(8693), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/813.jpg", new DateTime(2021, 5, 20, 11, 13, 14, 770, DateTimeKind.Local).AddTicks(8698) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 5, 20, 11, 13, 14, 770, DateTimeKind.Local).AddTicks(8716), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/965.jpg", new DateTime(2021, 5, 20, 11, 13, 14, 770, DateTimeKind.Local).AddTicks(8721) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 5, 20, 11, 13, 14, 770, DateTimeKind.Local).AddTicks(8740), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/1187.jpg", new DateTime(2021, 5, 20, 11, 13, 14, 770, DateTimeKind.Local).AddTicks(8745) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 5, 20, 11, 13, 14, 770, DateTimeKind.Local).AddTicks(8763), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/443.jpg", new DateTime(2021, 5, 20, 11, 13, 14, 770, DateTimeKind.Local).AddTicks(8768) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 5, 20, 11, 13, 14, 770, DateTimeKind.Local).AddTicks(8787), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/857.jpg", new DateTime(2021, 5, 20, 11, 13, 14, 770, DateTimeKind.Local).AddTicks(8792) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 5, 20, 11, 13, 14, 770, DateTimeKind.Local).AddTicks(8810), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/885.jpg", new DateTime(2021, 5, 20, 11, 13, 14, 770, DateTimeKind.Local).AddTicks(8815) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 5, 20, 11, 13, 14, 770, DateTimeKind.Local).AddTicks(8833), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/898.jpg", new DateTime(2021, 5, 20, 11, 13, 14, 770, DateTimeKind.Local).AddTicks(8838) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 5, 20, 11, 13, 14, 776, DateTimeKind.Local).AddTicks(2515), "https://picsum.photos/640/480/?image=34", new DateTime(2021, 5, 20, 11, 13, 14, 776, DateTimeKind.Local).AddTicks(3678) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 22,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 5, 20, 11, 13, 14, 776, DateTimeKind.Local).AddTicks(3959), "https://picsum.photos/640/480/?image=210", new DateTime(2021, 5, 20, 11, 13, 14, 776, DateTimeKind.Local).AddTicks(3967) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 23,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 5, 20, 11, 13, 14, 776, DateTimeKind.Local).AddTicks(4108), "https://picsum.photos/640/480/?image=938", new DateTime(2021, 5, 20, 11, 13, 14, 776, DateTimeKind.Local).AddTicks(4115) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 24,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 5, 20, 11, 13, 14, 776, DateTimeKind.Local).AddTicks(4141), "https://picsum.photos/640/480/?image=582", new DateTime(2021, 5, 20, 11, 13, 14, 776, DateTimeKind.Local).AddTicks(4147) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 25,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 5, 20, 11, 13, 14, 776, DateTimeKind.Local).AddTicks(4167), "https://picsum.photos/640/480/?image=200", new DateTime(2021, 5, 20, 11, 13, 14, 776, DateTimeKind.Local).AddTicks(4172) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 26,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 5, 20, 11, 13, 14, 776, DateTimeKind.Local).AddTicks(4192), "https://picsum.photos/640/480/?image=556", new DateTime(2021, 5, 20, 11, 13, 14, 776, DateTimeKind.Local).AddTicks(4197) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 27,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 5, 20, 11, 13, 14, 776, DateTimeKind.Local).AddTicks(4217), "https://picsum.photos/640/480/?image=895", new DateTime(2021, 5, 20, 11, 13, 14, 776, DateTimeKind.Local).AddTicks(4223) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 28,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 5, 20, 11, 13, 14, 776, DateTimeKind.Local).AddTicks(4243), "https://picsum.photos/640/480/?image=501", new DateTime(2021, 5, 20, 11, 13, 14, 776, DateTimeKind.Local).AddTicks(4248) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 29,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 5, 20, 11, 13, 14, 776, DateTimeKind.Local).AddTicks(4268), "https://picsum.photos/640/480/?image=779", new DateTime(2021, 5, 20, 11, 13, 14, 776, DateTimeKind.Local).AddTicks(4273) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 30,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 5, 20, 11, 13, 14, 776, DateTimeKind.Local).AddTicks(4292), "https://picsum.photos/640/480/?image=971", new DateTime(2021, 5, 20, 11, 13, 14, 776, DateTimeKind.Local).AddTicks(4297) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 31,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 5, 20, 11, 13, 14, 776, DateTimeKind.Local).AddTicks(4316), "https://picsum.photos/640/480/?image=677", new DateTime(2021, 5, 20, 11, 13, 14, 776, DateTimeKind.Local).AddTicks(4321) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 32,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 5, 20, 11, 13, 14, 776, DateTimeKind.Local).AddTicks(4340), "https://picsum.photos/640/480/?image=987", new DateTime(2021, 5, 20, 11, 13, 14, 776, DateTimeKind.Local).AddTicks(4345) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 33,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 5, 20, 11, 13, 14, 776, DateTimeKind.Local).AddTicks(4365), "https://picsum.photos/640/480/?image=842", new DateTime(2021, 5, 20, 11, 13, 14, 776, DateTimeKind.Local).AddTicks(4371) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 34,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 5, 20, 11, 13, 14, 776, DateTimeKind.Local).AddTicks(4390), "https://picsum.photos/640/480/?image=127", new DateTime(2021, 5, 20, 11, 13, 14, 776, DateTimeKind.Local).AddTicks(4395) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 35,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 5, 20, 11, 13, 14, 776, DateTimeKind.Local).AddTicks(4415), "https://picsum.photos/640/480/?image=312", new DateTime(2021, 5, 20, 11, 13, 14, 776, DateTimeKind.Local).AddTicks(4420) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 36,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 5, 20, 11, 13, 14, 776, DateTimeKind.Local).AddTicks(4480), "https://picsum.photos/640/480/?image=122", new DateTime(2021, 5, 20, 11, 13, 14, 776, DateTimeKind.Local).AddTicks(4485) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 37,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 5, 20, 11, 13, 14, 776, DateTimeKind.Local).AddTicks(4509), "https://picsum.photos/640/480/?image=416", new DateTime(2021, 5, 20, 11, 13, 14, 776, DateTimeKind.Local).AddTicks(4514) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 38,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 5, 20, 11, 13, 14, 776, DateTimeKind.Local).AddTicks(4534), "https://picsum.photos/640/480/?image=127", new DateTime(2021, 5, 20, 11, 13, 14, 776, DateTimeKind.Local).AddTicks(4539) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 39,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 5, 20, 11, 13, 14, 776, DateTimeKind.Local).AddTicks(4558), "https://picsum.photos/640/480/?image=49", new DateTime(2021, 5, 20, 11, 13, 14, 776, DateTimeKind.Local).AddTicks(4563) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 40,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 5, 20, 11, 13, 14, 776, DateTimeKind.Local).AddTicks(4582), "https://picsum.photos/640/480/?image=579", new DateTime(2021, 5, 20, 11, 13, 14, 776, DateTimeKind.Local).AddTicks(4587) });

            migrationBuilder.InsertData(
                table: "PostReactions",
                columns: new[] { "Id", "CreatedAt", "IsLike", "PostId", "UpdatedAt", "UserId" },
                values: new object[,]
                {
                    { 20, new DateTime(2021, 5, 20, 11, 13, 15, 54, DateTimeKind.Local).AddTicks(9811), false, 8, new DateTime(2021, 5, 20, 11, 13, 15, 54, DateTimeKind.Local).AddTicks(9817), 11 },
                    { 1, new DateTime(2021, 5, 20, 11, 13, 15, 54, DateTimeKind.Local).AddTicks(7499), true, 2, new DateTime(2021, 5, 20, 11, 13, 15, 54, DateTimeKind.Local).AddTicks(8513), 14 },
                    { 19, new DateTime(2021, 5, 20, 11, 13, 15, 54, DateTimeKind.Local).AddTicks(9779), true, 12, new DateTime(2021, 5, 20, 11, 13, 15, 54, DateTimeKind.Local).AddTicks(9784), 7 },
                    { 18, new DateTime(2021, 5, 20, 11, 13, 15, 54, DateTimeKind.Local).AddTicks(9744), false, 18, new DateTime(2021, 5, 20, 11, 13, 15, 54, DateTimeKind.Local).AddTicks(9750), 2 }
                });

            migrationBuilder.InsertData(
                table: "PostReactions",
                columns: new[] { "Id", "CreatedAt", "IsLike", "PostId", "UpdatedAt", "UserId" },
                values: new object[,]
                {
                    { 17, new DateTime(2021, 5, 20, 11, 13, 15, 54, DateTimeKind.Local).AddTicks(9713), true, 18, new DateTime(2021, 5, 20, 11, 13, 15, 54, DateTimeKind.Local).AddTicks(9718), 4 },
                    { 16, new DateTime(2021, 5, 20, 11, 13, 15, 54, DateTimeKind.Local).AddTicks(9681), false, 19, new DateTime(2021, 5, 20, 11, 13, 15, 54, DateTimeKind.Local).AddTicks(9686), 12 },
                    { 14, new DateTime(2021, 5, 20, 11, 13, 15, 54, DateTimeKind.Local).AddTicks(9616), true, 17, new DateTime(2021, 5, 20, 11, 13, 15, 54, DateTimeKind.Local).AddTicks(9621), 10 },
                    { 13, new DateTime(2021, 5, 20, 11, 13, 15, 54, DateTimeKind.Local).AddTicks(9582), false, 8, new DateTime(2021, 5, 20, 11, 13, 15, 54, DateTimeKind.Local).AddTicks(9588), 1 },
                    { 12, new DateTime(2021, 5, 20, 11, 13, 15, 54, DateTimeKind.Local).AddTicks(9551), true, 8, new DateTime(2021, 5, 20, 11, 13, 15, 54, DateTimeKind.Local).AddTicks(9556), 2 },
                    { 11, new DateTime(2021, 5, 20, 11, 13, 15, 54, DateTimeKind.Local).AddTicks(9520), true, 17, new DateTime(2021, 5, 20, 11, 13, 15, 54, DateTimeKind.Local).AddTicks(9525), 16 },
                    { 15, new DateTime(2021, 5, 20, 11, 13, 15, 54, DateTimeKind.Local).AddTicks(9649), false, 9, new DateTime(2021, 5, 20, 11, 13, 15, 54, DateTimeKind.Local).AddTicks(9654), 20 },
                    { 9, new DateTime(2021, 5, 20, 11, 13, 15, 54, DateTimeKind.Local).AddTicks(9452), false, 8, new DateTime(2021, 5, 20, 11, 13, 15, 54, DateTimeKind.Local).AddTicks(9457), 18 },
                    { 8, new DateTime(2021, 5, 20, 11, 13, 15, 54, DateTimeKind.Local).AddTicks(9417), false, 4, new DateTime(2021, 5, 20, 11, 13, 15, 54, DateTimeKind.Local).AddTicks(9423), 2 },
                    { 7, new DateTime(2021, 5, 20, 11, 13, 15, 54, DateTimeKind.Local).AddTicks(9383), true, 10, new DateTime(2021, 5, 20, 11, 13, 15, 54, DateTimeKind.Local).AddTicks(9389), 8 },
                    { 6, new DateTime(2021, 5, 20, 11, 13, 15, 54, DateTimeKind.Local).AddTicks(9350), true, 19, new DateTime(2021, 5, 20, 11, 13, 15, 54, DateTimeKind.Local).AddTicks(9355), 14 },
                    { 10, new DateTime(2021, 5, 20, 11, 13, 15, 54, DateTimeKind.Local).AddTicks(9487), true, 13, new DateTime(2021, 5, 20, 11, 13, 15, 54, DateTimeKind.Local).AddTicks(9492), 18 },
                    { 5, new DateTime(2021, 5, 20, 11, 13, 15, 54, DateTimeKind.Local).AddTicks(9315), false, 5, new DateTime(2021, 5, 20, 11, 13, 15, 54, DateTimeKind.Local).AddTicks(9321), 8 },
                    { 4, new DateTime(2021, 5, 20, 11, 13, 15, 54, DateTimeKind.Local).AddTicks(9281), true, 4, new DateTime(2021, 5, 20, 11, 13, 15, 54, DateTimeKind.Local).AddTicks(9287), 6 },
                    { 3, new DateTime(2021, 5, 20, 11, 13, 15, 54, DateTimeKind.Local).AddTicks(9248), true, 4, new DateTime(2021, 5, 20, 11, 13, 15, 54, DateTimeKind.Local).AddTicks(9254), 1 },
                    { 2, new DateTime(2021, 5, 20, 11, 13, 15, 54, DateTimeKind.Local).AddTicks(9192), true, 17, new DateTime(2021, 5, 20, 11, 13, 15, 54, DateTimeKind.Local).AddTicks(9203), 5 }
                });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 8, "Ipsa laborum accusantium saepe. Sed numquam sit inventore beatae cumque voluptas et dignissimos voluptates. Earum expedita numquam harum quo voluptatem velit.", new DateTime(2021, 5, 20, 11, 13, 15, 36, DateTimeKind.Local).AddTicks(1719), 40, new DateTime(2021, 5, 20, 11, 13, 15, 36, DateTimeKind.Local).AddTicks(2872) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 18, "mollitia", new DateTime(2021, 5, 20, 11, 13, 15, 36, DateTimeKind.Local).AddTicks(4623), 30, new DateTime(2021, 5, 20, 11, 13, 15, 36, DateTimeKind.Local).AddTicks(4658) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 19, "voluptatum", new DateTime(2021, 5, 20, 11, 13, 15, 36, DateTimeKind.Local).AddTicks(4872), 31, new DateTime(2021, 5, 20, 11, 13, 15, 36, DateTimeKind.Local).AddTicks(4882) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 12, "Ut eaque aut sunt occaecati autem. Rerum qui neque in in. Quia ullam fugiat optio unde ex dolor velit quasi unde. Id id ea reiciendis id sit blanditiis assumenda quisquam. Doloribus et est quia fugit ipsa.", new DateTime(2021, 5, 20, 11, 13, 15, 36, DateTimeKind.Local).AddTicks(5624), 38, new DateTime(2021, 5, 20, 11, 13, 15, 36, DateTimeKind.Local).AddTicks(5651) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 19, "Adipisci provident cumque repellendus facilis qui quia quos. Ipsa rem corrupti ut corrupti repellendus et qui eaque sapiente. Et soluta dolores magni qui sit in iusto.", new DateTime(2021, 5, 20, 11, 13, 15, 36, DateTimeKind.Local).AddTicks(5977), 27, new DateTime(2021, 5, 20, 11, 13, 15, 36, DateTimeKind.Local).AddTicks(6025) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 3, "Inventore voluptate quia distinctio consectetur. Commodi dolores minima omnis velit occaecati esse esse quidem voluptas. Quos occaecati at deleniti dicta sint aperiam. Reprehenderit laudantium et rerum dolor ab amet.", new DateTime(2021, 5, 20, 11, 13, 15, 36, DateTimeKind.Local).AddTicks(6354), 25, new DateTime(2021, 5, 20, 11, 13, 15, 36, DateTimeKind.Local).AddTicks(6364) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 17, "beatae", new DateTime(2021, 5, 20, 11, 13, 15, 36, DateTimeKind.Local).AddTicks(6426), 32, new DateTime(2021, 5, 20, 11, 13, 15, 36, DateTimeKind.Local).AddTicks(6432) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 12, "vitae", new DateTime(2021, 5, 20, 11, 13, 15, 36, DateTimeKind.Local).AddTicks(6487), 32, new DateTime(2021, 5, 20, 11, 13, 15, 36, DateTimeKind.Local).AddTicks(6493) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 21, "Dicta est reiciendis voluptas earum odit cumque in suscipit in.", new DateTime(2021, 5, 20, 11, 13, 15, 36, DateTimeKind.Local).AddTicks(7857), 38, new DateTime(2021, 5, 20, 11, 13, 15, 36, DateTimeKind.Local).AddTicks(7883) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 6, "Et dolores sed. Aspernatur est modi nemo. Labore natus placeat quo quis.", new DateTime(2021, 5, 20, 11, 13, 15, 36, DateTimeKind.Local).AddTicks(8160), 29, new DateTime(2021, 5, 20, 11, 13, 15, 36, DateTimeKind.Local).AddTicks(8234) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 18, "Hic a velit optio corporis dolor voluptatum. At illum labore quia sit voluptate soluta ea. Voluptatum voluptatem aliquam dolor eos corporis soluta. Eum ex et aut eveniet qui ut. Veniam modi magni iure.", new DateTime(2021, 5, 20, 11, 13, 15, 36, DateTimeKind.Local).AddTicks(8574), 36, new DateTime(2021, 5, 20, 11, 13, 15, 36, DateTimeKind.Local).AddTicks(8583) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 19, "Quasi quis quos illo aperiam autem nisi.", new DateTime(2021, 5, 20, 11, 13, 15, 36, DateTimeKind.Local).AddTicks(8727), 24, new DateTime(2021, 5, 20, 11, 13, 15, 36, DateTimeKind.Local).AddTicks(8735) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 2, "Atque et dicta et minus ipsum et. Laborum ut tenetur tempore ut aut. Sit unde perferendis temporibus distinctio at quidem aut quos vel. Quos consequatur aut impedit tempora ab ea vel. At dolores deleniti expedita et saepe ipsa consequatur provident. Illum repellendus veritatis rem neque est.", new DateTime(2021, 5, 20, 11, 13, 15, 37, DateTimeKind.Local).AddTicks(1049), 38, new DateTime(2021, 5, 20, 11, 13, 15, 37, DateTimeKind.Local).AddTicks(1076) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 7, "quod", new DateTime(2021, 5, 20, 11, 13, 15, 37, DateTimeKind.Local).AddTicks(1193), 37, new DateTime(2021, 5, 20, 11, 13, 15, 37, DateTimeKind.Local).AddTicks(1200) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 18, "Soluta repudiandae velit.\nIpsam ut enim necessitatibus eius ab quae quasi ex neque.\nConsequatur et in architecto repellat voluptas molestiae provident pariatur.\nEx doloribus et.\nEt non ut quidem iste hic reiciendis et.\nNumquam excepturi quas.", new DateTime(2021, 5, 20, 11, 13, 15, 37, DateTimeKind.Local).AddTicks(2954), 38, new DateTime(2021, 5, 20, 11, 13, 15, 37, DateTimeKind.Local).AddTicks(2984) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 20, "Sed sint eveniet.", new DateTime(2021, 5, 20, 11, 13, 15, 37, DateTimeKind.Local).AddTicks(3100), 28, new DateTime(2021, 5, 20, 11, 13, 15, 37, DateTimeKind.Local).AddTicks(3108) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 18, "maxime", new DateTime(2021, 5, 20, 11, 13, 15, 37, DateTimeKind.Local).AddTicks(3161), 31, new DateTime(2021, 5, 20, 11, 13, 15, 37, DateTimeKind.Local).AddTicks(3168) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 2, "Vel error aut sapiente.", new DateTime(2021, 5, 20, 11, 13, 15, 37, DateTimeKind.Local).AddTicks(3249), 39, new DateTime(2021, 5, 20, 11, 13, 15, 37, DateTimeKind.Local).AddTicks(3256) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 11, "ipsam", new DateTime(2021, 5, 20, 11, 13, 15, 37, DateTimeKind.Local).AddTicks(3302), 31, new DateTime(2021, 5, 20, 11, 13, 15, 37, DateTimeKind.Local).AddTicks(3309) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 16, "Numquam temporibus dolor non commodi error quos earum officia.", new DateTime(2021, 5, 20, 11, 13, 15, 37, DateTimeKind.Local).AddTicks(3511), 23, new DateTime(2021, 5, 20, 11, 13, 15, 37, DateTimeKind.Local).AddTicks(3520) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 3, new DateTime(2021, 5, 20, 11, 13, 14, 826, DateTimeKind.Local).AddTicks(488), "Murray_Schuster79@gmail.com", "acAJ53wZsq31VAb6cMXgUfIax6/m7Y0/RHnoJEnMYEg=", "Rcm3LnHPlpiN5J9LLJYTd0PcilyDDnVc6Nswoyo47j0=", new DateTime(2021, 5, 20, 11, 13, 14, 826, DateTimeKind.Local).AddTicks(1583), "Titus_Kuvalis" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 13, new DateTime(2021, 5, 20, 11, 13, 14, 837, DateTimeKind.Local).AddTicks(138), "Dave79@yahoo.com", "d32nF9qsABrvxMVc32IrJVyR99sYzoFWb8KMrKJtcBo=", "tKnkVn1ZFt/SZzKzklyEc3+JSjAszT7v5RMwkfSSA6I=", new DateTime(2021, 5, 20, 11, 13, 14, 837, DateTimeKind.Local).AddTicks(212), "Al_Raynor" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 19, new DateTime(2021, 5, 20, 11, 13, 14, 846, DateTimeKind.Local).AddTicks(7785), "Kendrick_Emmerich13@gmail.com", "hwaghCHm2/1iUok1aCOO6BLTATiQihj5O2AH/ftjbRE=", "+uJsbanlGIkilFjT1yEIO2jLY+GJdMK/HVrxnuwP6i8=", new DateTime(2021, 5, 20, 11, 13, 14, 846, DateTimeKind.Local).AddTicks(7817), "Lenore16" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 20, new DateTime(2021, 5, 20, 11, 13, 14, 856, DateTimeKind.Local).AddTicks(5118), "Lelia_Wintheiser@gmail.com", "NT0Ru66Nr2SQMOYwTg7h8HFDkbhZSGnjrOhfKMCK/VY=", "bRn9pW/8YH842PoDeaPqCJs4ZzFupb6KLJ5cFgWvvXI=", new DateTime(2021, 5, 20, 11, 13, 14, 856, DateTimeKind.Local).AddTicks(5182), "Lionel74" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 2, new DateTime(2021, 5, 20, 11, 13, 14, 866, DateTimeKind.Local).AddTicks(1579), "Shanna.Wolf62@yahoo.com", "xB+JebeSFso9LfB1R0QWq2UD0oOg7UGYMRPoyZ6i77Y=", "mXrPmUfK+EPvjzmLLdmoZE3H6a37OmSXuCK1tH6cFso=", new DateTime(2021, 5, 20, 11, 13, 14, 866, DateTimeKind.Local).AddTicks(1657), "Natasha_Watsica1" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 4, new DateTime(2021, 5, 20, 11, 13, 14, 875, DateTimeKind.Local).AddTicks(6521), "Emil_Moen50@gmail.com", "Wgxx1H7JzjFDpm110weZvqoahZTXgr7Qe3b5WgxraQk=", "gohkEBKxAZWAwYeggEMnkbA6CM7XnSMXVBfwN82C910=", new DateTime(2021, 5, 20, 11, 13, 14, 875, DateTimeKind.Local).AddTicks(6589), "Jennifer45" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 9, new DateTime(2021, 5, 20, 11, 13, 14, 885, DateTimeKind.Local).AddTicks(1450), "Kaleb27@hotmail.com", "J1uf9/PUNeKVrn7jF/Yq7m+0EwZADBlvR+cV+rtcxZQ=", "ySLHbl4+0zW3baq+7fkLbdh9zsJFD7CH2FXuO9SBx58=", new DateTime(2021, 5, 20, 11, 13, 14, 885, DateTimeKind.Local).AddTicks(1515), "Hunter.Kemmer" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 5, new DateTime(2021, 5, 20, 11, 13, 14, 894, DateTimeKind.Local).AddTicks(4593), "Brycen_Bayer@gmail.com", "aaOiiDbTIa2LHpb3qRNhL89CnRUeVJxi+tSdrX6b4OI=", "I4Gidaxl1CccNkwj4MlceIvNOPqMd17wS2LXcrlqh04=", new DateTime(2021, 5, 20, 11, 13, 14, 894, DateTimeKind.Local).AddTicks(4653), "Chelsie24" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 8, new DateTime(2021, 5, 20, 11, 13, 14, 904, DateTimeKind.Local).AddTicks(2432), "Giuseppe.Schowalter35@gmail.com", "hZ+2DYV5XXAIOVHgnWBDbfpcg8AQ4zM+9wIJLyAb918=", "hcAUmWfOD0aISKN9HfNk26g/aFy0FNDtKhd/p4b3dO0=", new DateTime(2021, 5, 20, 11, 13, 14, 904, DateTimeKind.Local).AddTicks(2515), "Gino.White66" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 15, new DateTime(2021, 5, 20, 11, 13, 14, 913, DateTimeKind.Local).AddTicks(1972), "Madge.McKenzie27@hotmail.com", "wkwbVcA0aF/VIzL/IDIjM7Azl1hRLCpn60+08IGE6BU=", "0FdI7ApNn6iiqH+JugvWa51yvP0xQNkO1rRxsvQ2Dlo=", new DateTime(2021, 5, 20, 11, 13, 14, 913, DateTimeKind.Local).AddTicks(2019), "Warren_Bernier" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 8, new DateTime(2021, 5, 20, 11, 13, 14, 922, DateTimeKind.Local).AddTicks(3523), "Sandra58@hotmail.com", "k8R4dwnKEzCNF/gUJhFLkEv4xK5nI1GjvYZySMBXGfU=", "usYqevKzZb+qARv2wZCKcPL7vjZGIQf77wZaG9DWDk4=", new DateTime(2021, 5, 20, 11, 13, 14, 922, DateTimeKind.Local).AddTicks(3584), "Angelina.Krajcik76" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 10, new DateTime(2021, 5, 20, 11, 13, 14, 931, DateTimeKind.Local).AddTicks(4810), "Etha.Kuhic@hotmail.com", "tUkmuUC33oqMFIku6hXOpevaYAFHpD2+MLu02CIEfYg=", "xM96YjEgTurGXzj8FfvB3DIj8YsMlioDF0vJrWGj63c=", new DateTime(2021, 5, 20, 11, 13, 14, 931, DateTimeKind.Local).AddTicks(4874), "Antonio.Kub" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 4, new DateTime(2021, 5, 20, 11, 13, 14, 940, DateTimeKind.Local).AddTicks(5517), "Reymundo.Hyatt9@hotmail.com", "0t+8Uu4780g97Q47WrMGjg4ZC2NyaZyjhrVAueDckpw=", "k+nKIvhRcAZoCTlvp4jeLaxOc6Mm7mQvpAZpwIXGiZA=", new DateTime(2021, 5, 20, 11, 13, 14, 940, DateTimeKind.Local).AddTicks(5579), "Buck_Reynolds58" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 8, new DateTime(2021, 5, 20, 11, 13, 14, 949, DateTimeKind.Local).AddTicks(9808), "Ewald_Brown15@gmail.com", "nWDeaY8L/6N+Fe0PIwDMFcbtD8CymNncpqw8sms9i7s=", "ubu59TXbBb0pWuax4nkHvFdu1BLWz6lpSf27whkn0Zw=", new DateTime(2021, 5, 20, 11, 13, 14, 949, DateTimeKind.Local).AddTicks(9852), "Nichole.Kulas" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 14, new DateTime(2021, 5, 20, 11, 13, 14, 960, DateTimeKind.Local).AddTicks(4225), "Webster.Armstrong@yahoo.com", "LbmH1t3nBO0bGpMlURNDdHCbhfm4YFhBp6i8jvYW89g=", "8aLXS7QORAG4uDB+wL5UxChZxhszib/in2oWivPE3oY=", new DateTime(2021, 5, 20, 11, 13, 14, 960, DateTimeKind.Local).AddTicks(4300), "Sigurd_Koch" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 16, new DateTime(2021, 5, 20, 11, 13, 14, 970, DateTimeKind.Local).AddTicks(4430), "Chester.Gibson@yahoo.com", "VxSMljQua5wzDvMTEaHBXvqXg52XaKLJZ9FwWeAr64A=", "99Xcb7TKeygJf4WqTJHCz40J7et0ShfsSKdBaosYcIU=", new DateTime(2021, 5, 20, 11, 13, 14, 970, DateTimeKind.Local).AddTicks(4500), "Zetta_Corwin" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 9, new DateTime(2021, 5, 20, 11, 13, 14, 979, DateTimeKind.Local).AddTicks(94), "Cory22@hotmail.com", "cC/WtQdyOIXKFdNESGGD3+4le0trUPJBOjDBYWkwGSs=", "vIQ5cLovukqZYHZ7CGBut3nvr4rD4pqucGuIplghdSc=", new DateTime(2021, 5, 20, 11, 13, 14, 979, DateTimeKind.Local).AddTicks(117), "Harrison_Funk" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 14, new DateTime(2021, 5, 20, 11, 13, 14, 987, DateTimeKind.Local).AddTicks(9474), "Maximus.Schowalter@hotmail.com", "07nUGFmM3kNcnN5BVOql7SjpvK++FzyfLSDhRBW5uhc=", "AlXbT32FGJzaFlNqe7BeNZHBHbEVro2VwUaHkpWOo/k=", new DateTime(2021, 5, 20, 11, 13, 14, 987, DateTimeKind.Local).AddTicks(9547), "Magnolia_Klocko" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 19, new DateTime(2021, 5, 20, 11, 13, 14, 998, DateTimeKind.Local).AddTicks(5070), "Kathryn_Abshire82@yahoo.com", "YPW+idxIGypCR3HAHfvBYxoTJIrKQqywAgAdOTCkUDQ=", "aLSTRGtEVgAXEB7SxTJVGuz2WAyBd8+WJ2DHMPCQAaQ=", new DateTime(2021, 5, 20, 11, 13, 14, 998, DateTimeKind.Local).AddTicks(5100), "Amina.Skiles4" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 10, new DateTime(2021, 5, 20, 11, 13, 15, 9, DateTimeKind.Local).AddTicks(4440), "Javon.Hane5@yahoo.com", "qjQeVb5gyvGZR7JCCa3WcNpCJbbZC1msMQAAm8xzC88=", "sOIYRCoaEv1lNThIyXb7kFDIyRELtsXhnm+bAfZ5fj0=", new DateTime(2021, 5, 20, 11, 13, 15, 9, DateTimeKind.Local).AddTicks(4506), "Precious44" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "CreatedAt", "Password", "Salt", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 5, 20, 11, 13, 15, 20, DateTimeKind.Local).AddTicks(3327), "UGZorU7ixmt7ZHZpwbpmfxeeDK1RdaxwQ8tmD7ojIGU=", "lAdg0m8nOXEvf6zmjZ8bbXKHhE8QfSpTUcV9L09Z+84=", new DateTime(2021, 5, 20, 11, 13, 15, 20, DateTimeKind.Local).AddTicks(3327) });
        }
    }
}
