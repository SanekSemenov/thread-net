﻿using System;
using AutoMapper;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Bogus.DataSets;
using Thread_.NET.BLL.Hubs;
using Thread_.NET.BLL.Services.Abstract;
using Thread_.NET.Common.DTO.Post;
using Thread_.NET.DAL.Context;
using Thread_.NET.DAL.Entities;

namespace Thread_.NET.BLL.Services
{
    public sealed class PostService : BaseService
    {
        private readonly IHubContext<PostHub> _postHub;

        public PostService(ThreadContext context, IMapper mapper, IHubContext<PostHub> postHub) : base(context, mapper)
        {
            _postHub = postHub;
        }

        public async Task<ICollection<PostDTO>> GetAllPosts()
        {
            var posts = await _context.Posts
                .Include(post => post.Author)
                    .ThenInclude(author => author.Avatar)
                .Include(post => post.Preview)
                .Include(post => post.Reactions)
                    .ThenInclude(reaction => reaction.User)
                .Include(post => post.Comments)
                    .ThenInclude(comment => comment.Reactions)
                .Include(post => post.Comments)
                    .ThenInclude(comment => comment.Author)
                .OrderByDescending(post => post.CreatedAt)
                .ToListAsync();

            return _mapper.Map<ICollection<PostDTO>>(posts);
        }

        public async Task<ICollection<PostDTO>> GetAllPosts(int userId)
        {
            var posts = await _context.Posts
                .Include(post => post.Author)
                    .ThenInclude(author => author.Avatar)
                .Include(post => post.Preview)
                .Include(post => post.Comments)
                    .ThenInclude(comment => comment.Author)
                .Where(p => p.AuthorId == userId) // Filter here
                .ToListAsync();

            return _mapper.Map<ICollection<PostDTO>>(posts);
        }

        public async Task<PostDTO> GetPostById(int postId)
        {
            /*var post = await _context.Posts
                .Where(x => x.Id == postId).FirstOrDefaultAsync();

            return _mapper.Map<PostDTO>(post);*/
            var post = await _context.Posts
                .Include(post => post.Author)
                .ThenInclude(author => author.Avatar)
                .Include(post => post.Preview)
                .Include(post => post.Reactions)
                .ThenInclude(reaction => reaction.User)
                .Include(post => post.Comments)
                .ThenInclude(comment => comment.Reactions)
                .Include(post => post.Comments)
                .ThenInclude(comment => comment.Author)
                .OrderByDescending(post => post.CreatedAt)
                .Where(x => x.Id == postId)
                .FirstOrDefaultAsync();
                //.ToListAsync();
                

            return _mapper.Map<PostDTO>(post);
        }
        public async Task<PostDTO> CreatePost(PostCreateDTO postDto)
        {
            var postEntity = _mapper.Map<Post>(postDto);

            _context.Posts.Add(postEntity);
            await _context.SaveChangesAsync();

            var createdPost = await _context.Posts
                .Include(post => post.Author)
					.ThenInclude(author => author.Avatar)
                .FirstAsync(post => post.Id == postEntity.Id);

            var createdPostDTO = _mapper.Map<PostDTO>(createdPost);
            await _postHub.Clients.All.SendAsync("NewPost", createdPostDTO);

            return createdPostDTO;
        }

        public async Task<PostDTO> UpdatePost(PostUpdateDTO postDto)
        {
            var postEntity = await _context.Posts.Where(x => x.Id == postDto.Id).FirstOrDefaultAsync();
            if (postEntity == null) return default;

            postEntity.Body = postDto.Body;
            //postEntity.Preview = postDto.PreviewImage;
            postEntity.UpdatedAt = DateTime.Now;
            
            await _context.SaveChangesAsync();

            return _mapper.Map<PostDTO>(postEntity);
        }

        public async Task<int> DeletePost(int postId)
        {
            var postEntity = await _context.Posts.Where(x => x.Id == postId).FirstOrDefaultAsync();
            if (postEntity == null) return default;

            _context.Posts.Remove(postEntity);
            await _context.SaveChangesAsync();

            return postEntity.Id;
        }

    }
}
