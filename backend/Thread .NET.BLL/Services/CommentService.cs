﻿using System;
using System.Linq;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using Thread_.NET.BLL.Services.Abstract;
using Thread_.NET.Common.DTO.Comment;
using Thread_.NET.DAL.Context;
using Thread_.NET.DAL.Entities;

namespace Thread_.NET.BLL.Services
{
    public sealed class CommentService : BaseService
    {
        public CommentService(ThreadContext context, IMapper mapper) : base(context, mapper) { }

        public async Task<CommentDTO> CreateComment(NewCommentDTO newComment)
        {
            var commentEntity = _mapper.Map<Comment>(newComment);

            _context.Comments.Add(commentEntity);
            await _context.SaveChangesAsync();

            var createdComment = await _context.Comments
                .Include(comment => comment.Author)
                    .ThenInclude(user => user.Avatar)
                .FirstAsync(comment => comment.Id == commentEntity.Id);

            return _mapper.Map<CommentDTO>(createdComment);
        }

        public async Task<CommentDTO> UpdateComment(CommentUpdateDTO updatedComment)
        {
            var commentEntity =
                await _context.Comments.Where(x => x.Id == updatedComment.Id).FirstOrDefaultAsync();

            if (commentEntity == null) return default;

            commentEntity.Body = updatedComment.Body;
            commentEntity.UpdatedAt = DateTime.Now;
            await _context.SaveChangesAsync();

            return _mapper.Map<CommentDTO>(commentEntity);
        }

        public async Task<int> DeleteComment(int id)
        {
            var commentEntity = await _context.Comments.Where(x => x.Id == id).FirstOrDefaultAsync();
            if (commentEntity == null) return default;

            _context.Comments.Remove(commentEntity);
            await _context.SaveChangesAsync();

            return commentEntity.Id;
        }
    }
}
