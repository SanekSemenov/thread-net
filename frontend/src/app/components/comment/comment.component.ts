import { Component, Input } from '@angular/core';
import { Comment } from '../../models/comment/comment';
import { UpdateComment } from '../../models/comment/update-comment'
import { CommentService } from '../../services/comment.service';
import { User } from '../../models/user';
import { catchError, switchMap, takeUntil } from 'rxjs/operators';
import { AuthenticationService } from '../../services/auth.service';
import { AuthDialogService } from '../../services/auth-dialog.service';
import { empty, Observable, Subject } from 'rxjs';
import { DialogType } from '../../models/common/auth-dialog-type';
import { LikeService } from '../../services/like.service';
import { PostComponent } from '../post/post.component';
import { Reaction } from 'src/app/models/reactions/reaction';

@Component({
    selector: 'app-comment',
    templateUrl: './comment.component.html',
    styleUrls: ['./comment.component.sass']
})
export class CommentComponent {
    @Input() public comment: Comment;
    @Input() public currentUser: User;

    somenumber: number;

    private unsubscribe$ = new Subject<void>();

    public constructor(
        private commentService: CommentService,
        private authService: AuthenticationService,
        private authDialogService: AuthDialogService,
        private likeService: LikeService,
        private postComponent: PostComponent
    ) { }

    public updatePost() {
        this.commentService.updatePost(this.comment).subscribe();

        this.cancel();
    }

    editDraft(post: Comment) {
        console.log(post);
        this.somenumber = post.id;
    }

    cancel() {
        this.somenumber = 0;
    }

    public deleteComment(comment: Comment) {
        this.postComponent.deleteComment(comment);
    }

    public likesCount(): number {
        return this.comment.reactions.filter(x => x.isLike === true).length;
    }

    public dislikesCount(): number {
        return this.comment.reactions.filter(x => x.isDisLike === true).length;
    }

    public commentLikeReactions(): Array<Reaction> {
        return this.comment.reactions.filter(x => x.isLike == true);
    }

    public commentDisLikeReactions(): Array<Reaction> {
        return this.comment.reactions.filter(x => x.isDisLike == true);
    }

    public likeIsDisabled(): boolean {
        if (this.comment.reactions.some(x => x.isLike == true && x.user.id == this.currentUser.id)) {
            return true;
        }
        return false;
    }

    public dislikeIsDisabled(): boolean {
        if (this.comment.reactions.some(x => x.isDisLike == true && x.user.id === this.currentUser.id)) {
            return true;
        }
        return false;
    }

    public likeComment() {
        if (!this.currentUser) {
            this.catchErrorWrapper(this.authService.getUser())
                .pipe(
                    switchMap((userResp) => this.likeService.likeComment(this.comment, userResp)),
                    takeUntil(this.unsubscribe$)
                )
                .subscribe((post) => (this.comment = post));

            return;
        }

        this.likeService
            .likeComment(this.comment, this.currentUser)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((comment) => (this.comment = comment));

        console.log(this.comment.reactions);
    }

    public dislikeComment() {
        if (!this.currentUser) {
            this.catchErrorWrapper(this.authService.getUser())
                .pipe(
                    switchMap((userResp) => this.likeService.dislikeComment(this.comment, userResp)),
                    takeUntil(this.unsubscribe$)
                )
                .subscribe((post) => (this.comment = post));

            return;
        }

        this.likeService
            .dislikeComment(this.comment, this.currentUser)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((comment) => (this.comment = comment));
    }

    public openAuthDialog() {
        this.authDialogService.openAuthDialog(DialogType.SignIn);
    }

    private catchErrorWrapper(obs: Observable<User>) {
        return obs.pipe(
            catchError(() => {
                this.openAuthDialog();
                return empty();
            })
        );
    }
}

