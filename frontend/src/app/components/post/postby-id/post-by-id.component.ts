import { Component, Input, OnInit, OnDestroy } from '@angular/core';
import { Post } from '../../../models/post/post';
import { AuthenticationService } from '../../../services/auth.service';
import { AuthDialogService } from '../../../services/auth-dialog.service';
import { empty, Observable, Subject } from 'rxjs';
import { DialogType } from '../../../models/common/auth-dialog-type';
import { LikeService } from '../../../services/like.service';
import { NewComment } from '../../../models/comment/new-comment';
import { CommentService } from '../../../services/comment.service';
import { User } from '../../../models/user';
import { Comment } from '../../../models/comment/comment';
import { catchError, switchMap, takeUntil } from 'rxjs/operators';
import { SnackBarService } from '../../../services/snack-bar.service';
import { PostService } from 'src/app/services/post.service';
import { Reaction } from '../../../models/reactions/reaction';
import { MainThreadComponent } from '../../main-thread/main-thread.component';
import { ToastrService } from 'ngx-toastr';
import { CommentComponent } from '../../comment/comment.component';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
    selector: 'app-post-by-id',
    templateUrl: './post-by-id.component.html',
    styleUrls: ['./post-by-id.component.sass']
})
export class PostByIdComponent implements OnDestroy {
    // @Input() public post: Post;
    @Input() public currentUser: User;

    somenumber: number;
    likeCount: number;
    dislikeCount: number;
    postReaction: Reaction;
    disLikeReaction: boolean;
    id: number;
    post: Post;

    public showComments = false;
    public newComment = {} as NewComment;

    private unsubscribe$ = new Subject<void>();

    public constructor(
        private authService: AuthenticationService,
        private authDialogService: AuthDialogService,
        private likeService: LikeService,
        private commentService: CommentService,
        private snackBarService: SnackBarService,
        private postService: PostService,
        private mainThreadComponent: MainThreadComponent,
        private toastr: ToastrService,
        activeRoute: ActivatedRoute

    ) { this.id = Number.parseInt(activeRoute.snapshot.params["id"]) }

    public ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }

    public ngOnInit() {
        this.getPost(this.id);

    }

    getPost(id: number) {
        //this.loadingPosts = true;
        this.postService
            .getPostById(id)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(

            );
    }

    public toggleComments() {
        if (!this.currentUser) {
            this.catchErrorWrapper(this.authService.getUser())
                .pipe(takeUntil(this.unsubscribe$))
                .subscribe((user) => {
                    if (user) {
                        this.currentUser = user;
                        this.showComments = !this.showComments;
                    }
                });
            return;
        }

        this.showComments = !this.showComments;
    }

    public likeIsDisabled(): boolean {
        if (this.post.reactions.some(x => x.isLike == true && x.user.id === this.currentUser.id)) {
            return true;
        }
        return false;
    }

    public dislikeIsDisabled(): boolean {
        if (this.post.reactions.some(x => x.isDisLike == true && x.user.id === this.currentUser.id)) {
            return true;
        }
        return false;
    }

    public postLikeReactions(): Array<Reaction> {
        return this.post.reactions.filter(x => x.isLike == true);
    }

    public postDisLikeReactions(): Array<Reaction> {
        return this.post.reactions.filter(x => x.isDisLike == true);
    }

    public likesCount(): number {
        return this.post.reactions.filter(x => x.isLike === true).length;
    }

    public dislikesCount(): number {
        return this.post.reactions.filter(x => x.isDisLike === true).length;
    }

    public updatePost() {
        this.postService.updatePost(this.post).subscribe(() => console.log(this.post));

        this.likeService
            .likePost(this.post, this.currentUser)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((post) => (this.post = post));

        this.cancel();
    }

    editDraft(post: Post) {
        this.somenumber = post.id;
        console.log(this.post.reactions);
        //this.draft = draft;

    }

    cancel() {
        console.log(this.post.reactions);
        this.somenumber = 0;
    }

    public deletePost(post: Post) {
        this.postService.deletePost(this.post)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(
                () => {
                    this.mainThreadComponent.deletePost(post);
                    this.toastr.warning('Post deleted');
                },
                (error) => this.snackBarService.showErrorMessage(error)
            );
    }


    public likePost() {
        console.log(this.post.reactions);
        this.likeIsDisabled();
        if (!this.currentUser) {
            this.catchErrorWrapper(this.authService.getUser())
                .pipe(
                    switchMap((userResp) => this.likeService.likePost(this.post, userResp)),
                    takeUntil(this.unsubscribe$)
                )
                .subscribe((post) => (this.post = post));

            return;

        }

        this.likeService
            .likePost(this.post, this.currentUser)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((post) => (this.post = post));
    }

    public dislikePost() {
        if (!this.currentUser) {
            this.catchErrorWrapper(this.authService.getUser())
                .pipe(
                    switchMap((userResp) => this.likeService.dislikePost(this.post, userResp)),
                    takeUntil(this.unsubscribe$)
                )
                .subscribe((post) => (this.post = post));

            return;
        }

        this.likeService
            .dislikePost(this.post, this.currentUser)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((post) => (this.post = post));
    }

    public sendComment() {
        this.newComment.authorId = this.currentUser.id;
        this.newComment.postId = this.post.id;

        this.commentService
            .createComment(this.newComment)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(
                (resp) => {
                    if (resp) {
                        this.post.comments = this.sortCommentArray(this.post.comments.concat(resp.body));
                        this.newComment.body = undefined;
                    }
                },
                (error) => this.snackBarService.showErrorMessage(error)
            );
    }

    public deleteComment(comment: Comment) {
        this.commentService
            .deletePost(comment)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(
                () => {
                    this.post.comments = this.sortCommentArray(this.post.comments.filter(x => x.id !== comment.id));
                },
                (error) => this.snackBarService.showErrorMessage(error)
            )
    }

    public openAuthDialog() {
        this.authDialogService.openAuthDialog(DialogType.SignIn);
    }

    private catchErrorWrapper(obs: Observable<User>) {
        return obs.pipe(
            catchError(() => {
                this.openAuthDialog();
                return empty();
            })
        );
    }

    private sortCommentArray(array: Comment[]): Comment[] {
        return array.sort((a, b) => +new Date(b.createdAt) - +new Date(a.createdAt));
    }
}
