import { User } from '../user';

export interface UpdateComment {
    id: number;
    author: User;
    body: string;
}
