export interface UpdatePost {
    id: number;
    previewImage: string;
    body: string;
}
