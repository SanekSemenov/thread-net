export interface NewReaction {
    entityId: number;
    isLike: boolean;
    isDisLike: boolean;
    userId: number;
}
