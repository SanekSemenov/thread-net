import { Injectable } from '@angular/core';
import { HttpInternalService } from './http-internal.service';
import { Post } from '../models/post/post';
import { NewReaction } from '../models/reactions/newReaction';
import { NewPost } from '../models/post/new-post';
import { UpdatePost } from '../models/post/update-post';

@Injectable({ providedIn: 'root' })
export class PostService {
    public routePrefix = '/api/posts';

    constructor(private httpService: HttpInternalService) { }

    public getPosts() {
        return this.httpService.getFullRequest<Post[]>(`${this.routePrefix}`);
    }

    public getPostById(id: number) {
        return this.httpService.getFullRequest<Post>(`${this.routePrefix}/` + id);
    }

    public createPost(post: NewPost) {
        return this.httpService.postFullRequest<Post>(`${this.routePrefix}`, post);
    }

    public updatePost(post: UpdatePost) {
        return this.httpService.putFullRequest<Post>(`${this.routePrefix}`, post);
    }

    public deletePost(post: UpdatePost) {
        console.log(`${this.routePrefix}` + '/' + post.id);
        return this.httpService.deleteFullRequest<Post>(`${this.routePrefix}/` + post.id);
    }

    public likePost(reaction: NewReaction) {
        return this.httpService.postFullRequest<Post>(`${this.routePrefix}/like`, reaction);
    }

    public dislikePost(reaction: NewReaction) {
        return this.httpService.postFullRequest<Post>(`${this.routePrefix}/dislike`, reaction);
    }
}
